var Diarymodel           = require('../app/backendapi/authapi/diary/model/diary');
var Usermodel           = require('../app/backendapi/unauthapi/user/model/user');
var commonFunction={
  /*** Get age using date of birth ***/
  getAgeUsingDob:function(dateString){
        var now = new Date();
        var today = new Date(now.getYear(),now.getMonth(),now.getDate());
        var yearNow = now.getYear();
        var monthNow = now.getMonth();
        var dateNow = now.getDate();
        var dob = new Date(dateString.substring(0,4),
                         dateString.substring(5,7)-1,
                         dateString.substring(8,10)
                         );
        var yearDob = dob.getYear();
        var monthDob = dob.getMonth();
        var dateDob = dob.getDate();
        var age = {};
        yearAge = yearNow - yearDob;
        if (monthNow >= monthDob)
          var monthAge = monthNow - monthDob;
        else {
          yearAge--;
          var monthAge = 12 + monthNow -monthDob;
        }
        if (dateNow >= dateDob)
          var dateAge = dateNow - dateDob;
        else {
          monthAge--;
          var dateAge = 31 + dateNow - dateDob;
          if (monthAge < 0) {
              monthAge = 11;
              yearAge--;
          }
        }
        age = {
            years: yearAge,
            months: monthAge,
            days: dateAge
        };
        ageString = age.years +"-"+ age.months +"-"+ age.days;
        return ageString;
  },
  /*** Check if data the user is allowed to post records and comments ***/
  checkRules(type,user_id,cb){
    //var d = new Date(new Date().setDate(new Date().getDate() - 30));
    // var d = new Date();
    // var day = d.getDate();
    // var month= d.getMonth()+1;
    // var year = d.getFullYear();
    var condiction = {
        id:user_id,
        category  :'B'
    //    'created <'  : year+"-"+month+"-"+year+" 00:00:00"
    };
    //console.log(condiction);
    switch (type) {
      case "recordPost":
      Usermodel.getUserByCondiction(condiction,function(err){console.log(err)},function(user){
        if(user.length>0){
          var today_date = new Date();
          var datetime = today_date.getFullYear()+"-"+(today_date.getMonth()+1)+"-"+today_date.getDate()+" 60:60:60"
          var recordcond = {
            user_id:user_id,
            'created <=':datetime,
            status : 1
          }
          console.log(recordcond);
          Diarymodel.getRecordByCondiction(recordcond,function(err){console.log(err)},function(records){
            if(records.length>4){
              cb(0);
            }else{
              //console.log(4-records.length);
              cb(5-records.length)
            }
          })
        }else{
          cb(true);
        }
      })
      break;
      case "sharePost":
      Usermodel.getUserByCondiction(condiction,function(err){console.log(err)},function(user){
        if(user.length>0){
          var today_date = new Date();
          var datetime = today_date.getFullYear()+"-"+(today_date.getMonth()+1)+"-"+today_date.getDate()+" 00:00:00"
          var sharecond = {
            user_id:user_id,
            'created >':datetime
          }
          Diarymodel.getShareByCondiction(sharecond,function(err){console.log(err)},function(shares){
            if(shares.length>10){
              cb(false);
            }else{
              cb(true);
            }
          })
        }else{
          cb(true);
        }
      })
      break;
  }
  }

}
module.exports=commonFunction;
