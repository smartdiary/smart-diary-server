var express         = require('express');
var path            = require('path');
var fs              = require('fs');
var router          = express.Router();
var app             = express();
var port            = 5000;
var mime            = require('mime');
var bodyParser      = require('body-parser');
//var engines         = require('consolidate');
/** Backend Api Controllers **/
var UnauthUser            = require('./app/backendapi/unauthapi/user/controller/user');
var AuthUser              = require('./app/backendapi/authapi/user/controller/user');
var Diary                 = require('./app/backendapi/authapi/diary/controller/diary');
/** End **/

/** Frontend Api Controllers **/
var FrontUnauthUser            = require('./app/frontendapi/unauthapi/user/controller/user');
var FrontAuthUser              = require('./app/frontendapi/authapi/user/controller/user');
/** End **/

/* App configuration Settings */
app.use(bodyParser.json());
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({
  extended : true,
  limit : '500mb'
}));

//
// app.engine('html', engines.mustache);
// app.set('view engine', 'html');

app.use(function(req, res, next) {
    res.header("Content-Type", "application/x-www-form-urlencoded");
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization,apikey,token");
    next();
});

app.get('/public/*', function(req,res){
    console.log(req.originalUrl);
    res.sendFile(path.join(__dirname, req.originalUrl));
});

/* Set the path for all controllers */
app.use('/api/unauthUser', UnauthUser);
app.use('/api/authUser', AuthUser);
app.use('/api/authDiary', Diary);

app.use('/front/unauthUser', FrontUnauthUser);
app.use('/front/authUser', FrontAuthUser);
/** End **/

app.listen(port , function() {
  console.log('Server started at port ' + port);
});
