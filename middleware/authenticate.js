var config       = require('../config/setting.js');
var Authmodel    = require('../app/backendapi/authapi/user/model/user');
var ERROR = {
    UNAUTH : {
        success : false,
        resCode : 401,
        response : "Unauthorized Access"
    },
    TOKENEXP : {
        success : false,
        resCode : 401,
        response :"Accesstoken Expire"
    },
    UNAUTHENTICATE  :{
        success : false,
        resCode : 401,
        response :  "Failed to authenticate token."
    }
};
function isAuthenticated(req, res, next) {
    // CHECK THE USER STORED IN SESSION FOR A CUSTOM VARIABLE
    // you can do this however you want with whatever variables you set up
    var token  = req.headers['token'];
    var apikey = req.headers['apikey'];
    var cond   = {
        token : token
    }
    console.log(cond);
    if(apikey==config.definedapikey){
      if (token) {
        Authmodel.getAccessTokenByCondiction(cond,function(err){console.log(err)},function(token){
          if(token.length>0){
            next();
          }else{
            res.status(200).send(ERROR.UNAUTHENTICATE);
          }
        })
      }else{
        res.status(200).json(ERROR.UNAUTH);
      }
    }else{
      res.status(200).send(ERROR.UNAUTH);
    }
}
module.exports=isAuthenticated;
