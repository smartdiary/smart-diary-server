module.exports={
   mail             : require('../lib/sendmail'),
   sendnotification : require('../lib/sendnotification'),
   helper           : require('../helper/commonhelper'),
   authenticate     : require('../middleware/authenticate'),
   validateapi      : require('../middleware/validateapi'),
   setting          : require('../config/setting')
}
