var express = require('express');
var router = express.Router();
var app = express();
var customodule = require('../../../../customexport');
var node_module = require('../../../../export');
var ERROR = require('../../../../../config/statuscode');
var Usermodel = require('../model/user');

//customodule.authenticate,

router.post('/changePassword', function (req, res, next) {
  var old_password = req.body.oldPassword;
  var new_password = req.body.newPassword;
  var email = req.body.email;
  var condiction = {
    password: node_module.crypto.createHash('md5').update(customodule.setting.passprefix + old_password).digest("hex"),
    email: email,
    status: '1'
  }
  var data = {
    password: node_module.crypto.createHash('md5').update(customodule.setting.passprefix + new_password).digest("hex"),
  }
  Usermodel.getUserByCondiction(condiction, function (err) { console.log(err) }, function (users) {
    if (users.length > 0) {
      Usermodel.updateUserByCondiction(data, condiction, function (err) { console.log(err) }, function (success) {
        if (success) {
          var response = {
            resCode: 200,
            response: "You have successfully change your password"
          }
          res.status(200).send(response);
        }
      })
    } else {
      res.status(200).send(ERROR.USER.INVALIDCREDENTIAL);
    }
  })
})


//*****************UPLOAD START USING ASYNC PACKAGE AND MULTER ***********//
var advertisestorage = node_module.multer.diskStorage({
  destination: function (req, file, cb) {
    //var code = JSON.parse(req.body.model).empCode;
    var dest = 'public/advertise/';
    node_module.mkdirp(dest, function (err) {
      console.log(dest);
      if (err) cb(err, dest);
      else cb(null, dest);
    });
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '.' + 'jpg');
  }
});
var advertiseimageupload = node_module.multer({
  storage: advertisestorage
});
//customodule.authenticate,
router.post('/addAdvertise', advertiseimageupload.any(), function (req, res, next) {
  var user_id = req.body.userId;
  var advt_name = req.body.name;
  var advt_desc = req.body.description;
  var start_date = req.body.startDate;
  var end_date = req.body.endDate;
  var post_date = req.body.post_date;

  console.log(req.files);
  console.log(req.body);
  console.log(req.file);
  if (req.files.length > 0) {
    var filename = req.files[0].filename;
  } else {
    var filename = '';
  }
  if (!advt_name || !advt_desc || !start_date || !user_id || !end_date) {
    res.status(200).send(ERROR.DIARY.REQUIREDFIELD);
  } else {
    var data = {
      user_id: user_id,
      advt_name: advt_name,
      advt_desc: advt_desc,
      start_date: start_date,
      end_date: end_date,
      post_date: post_date,
      advt_image: filename
    }
    Usermodel.addAdvertise(data, function (err) { console.log(err) }, function (success) {
      if (success) {
        var response = {
          resCode: 200,
          response: "Advertise created successfully"
        }
        res.status(200).send(response);
      }
    })
  }
});
//customodule.authenticate,
router.get('/deleteAdvertise', function (req, res, next) {
  var id = req.query.id;
  var condiction = {
    id: id
  }
  var data = {
    status: '0',
    is_archived: 'Y'
  }
  if (!id) {
    res.status(200).send(ERROR.DIARY.REQUIREDFIELD);
  } else {
    Usermodel.updateAdvertise(data, condiction, function (err) {
      res.status(200).send(ERROR.COMMON.SOMETHINGWRONG);
    }, function (updateshare) {
      var response = {
        resCode: 200,
        response: "Advertise has been deleted successfully"
      }
      res.status(200).send(response);
    })
  }
});
//customodule.authenticate,
router.put('/updateAdvertise', advertiseimageupload.any(), function (req, res, next) {
  var user_id = req.body.userId;
  var advt_name = req.body.name;
  var advt_desc = req.body.description;
  var start_date = req.body.startDate;
  var end_date = req.body.endDate;
  var advt_id = req.body.advertiseId;

  var price = req.body.price;
  var is_payment_set = req.body.is_payment_set;
  var is_published = req.body.is_published;
  var published_date = req.body.pulished_date;



  if (!advt_name || !advt_desc || !start_date || !end_date) {
    res.status(200).send(ERROR.DIARY.REQUIREDFIELD);
  } else {
    if (req.files.length > 0) {
      var filename = req.files[0].filename;
      if (user_id != '') {
        var data = {
          user_id: user_id,
          advt_name: advt_name,
          advt_desc: advt_desc,
          start_date: start_date,
          end_date: end_date,
          price: price,
          is_payment_set: is_payment_set,
          is_published: is_published,
          published_date: published_date,
          advt_image: filename
        }
      } else {
        var data = {
          advt_name: advt_name,
          advt_desc: advt_desc,
          start_date: start_date,
          end_date: end_date,
          price: price,
          is_payment_set: is_payment_set,
          is_published: is_published,
          published_date: published_date,
          advt_image: filename
        }
      }
    } else {
      if (user_id != '') {
        var data = {
          user_id: user_id,
          advt_name: advt_name,
          advt_desc: advt_desc,
          start_date: start_date,
          end_date: end_date,
          price: price,
          is_payment_set: is_payment_set,
          is_published: is_published,
          published_date: published_date,
        }
      } else {
        var data = {
          advt_name: advt_name,
          advt_desc: advt_desc,
          start_date: start_date,
          end_date: end_date,
          price: price,
          is_payment_set: is_payment_set,
          is_published: is_published,
          published_date: published_date,
        }
      }

    }

    var condiction = {
      id: advt_id,
      status: '1'
    }
    //console.log('data', data);
    //console.log('condiction', condiction);
    Usermodel.updateAdvertise(data, condiction, function (err) { res.status(200).send(ERROR.DIARY.REQUIREDFIELD); }, function (success) {
      if (success) {
        var response = {
          resCode: 200,
          response: "Advertise updated successfully"
        }
        res.status(200).send(response);
      }
    })
  }
})
//customodule.authenticate,
router.put('/updatePaymentAdvertise', advertiseimageupload.any(), function (req, res, next) {
  var user_id = req.body.userId;
  var advt_id = req.body.advertiseId;
  var is_paid = req.body.is_paid;


  if (!user_id) {
    res.status(200).send(ERROR.DIARY.REQUIREDFIELD);
  } else {
    var data = {
      user_id: user_id,
      is_paid: is_paid
    }

    var condiction = {
      id: advt_id,
      status: '1'
    }
    //console.log('data', data);
    //console.log('condiction', condiction);
    Usermodel.updateAdvertise(data, condiction, function (err) { res.status(200).send(ERROR.DIARY.REQUIREDFIELD); }, function (success) {
      if (success) {
        var response = {
          resCode: 200,
          response: "Advertise updated successfully"
        }
        res.status(200).send(response);
      }
    })
  }
})
//customodule.authenticate,
router.get('/getAdvertise', function (req, res, next) {
  var id = req.query.id;
  var type = req.query.type;

  var date = new Date(),
    mnth = ("0" + (date.getMonth() + 1)).slice(-2),
    day = ("0" + date.getDate()).slice(-2);
  var pre_date = [date.getFullYear(), mnth, day].join("-");

  // using by type :
  var is_payment_set = '';
  if (type == 'setPayment') {
    is_payment_set = 'N';
  }
  var is_paid = '';
  if (type == 'receivedPayment') {
    is_paid = 'N';
  }

  var is_published = '';
  if (type == 'activate') {
    is_published = 'N';
  }

  var condiction = {};
  // fetch by user id:
  if (id != '' && type == '') {
    condiction = {
      status: '1',
      user_id: id,
      is_archived: 'N'
    }
  } else if (id != '' && type != '') {
    if (type == 'setPayment') {
      condiction = {
        status: 1,
        user_id: id,
        is_archived: 'N',
        is_payment_set: is_payment_set
      }
    }
    if (type == 'receivedPayment') {
      condiction = {
        status: 1,
        user_id: id,
        is_archived: 'N',
        is_payment_set: 'Y',
        is_paid: is_paid
      }
    }
    if (type == 'activate') {
      condiction = {
        status: 1,
        user_id: id,
        is_archived: 'N',
        is_payment_set: 'Y',
        is_paid: 'Y',
        is_published: is_published,
      }
    }
    if (type == 'previous') {
      condiction = {
        status: 1,
        is_archived: 'N',
        'published_date +INTERVAL 9 DAY <=': pre_date
      }
    }
  } else if (id == '' && type != '') {
    if (type == 'setPayment') {
      condiction = {
        status: 1,
        is_archived: 'N',
        is_payment_set: is_payment_set
      }
    }
    if (type == 'receivedPayment') {
      condiction = {
        status: 1,
        is_archived: 'N',
        is_payment_set: 'Y',
        is_paid: is_paid
      }
    }
    if (type == 'activate') {
      condiction = {
        status: 1,
        is_archived: 'N',
        is_payment_set: 'Y',
        is_paid: 'Y',
        is_published: is_published,
      }
    }
    if (type == 'previous') {
      condiction = {
        status: 1,
        is_archived: 'N',
        'published_date +INTERVAL 9 DAY <=': pre_date
      }
    }
  } else {
    condiction = {
      status: '1',
      is_archived: 'N'
    }
  }
  console.log(condiction);
  Usermodel.getAdvertiseByCondiction(condiction, function (err) {
    res.status(200).send(ERROR.COMMON.SOMETHINGWRONG);
  }, function (advertise) {
    var response = {
      resCode: 200,
      path: customodule.setting.advertiseurl,
      response: advertise
    }
    res.status(200).send(response);
  })
});
//customodule.authenticate,
router.get('/getAdvertiseType', function (req, res, next) {
  var userId = req.query.userId;
  var condiction = {};
  var result = [];
  if (userId) {
    condiction = {
      user_id: userId
    }
  } else {
    condiction = {};
  }

  var con = {};
  //console.log(condiction);
  // get user count
  Usermodel.getUserTypeByCondiction(con, function (err) {
    res.status(200).send(ERROR.COMMON.SOMETHINGWRONG);
  }, function (user) {
    if (user.length > 0) {
      result.push(user);

      // get advertise count
      Usermodel.getAdvertiseTypeByCondiction(condiction, function (err) {
        res.status(200).send(ERROR.COMMON.SOMETHINGWRONG);
      }, function (advertise) {
        result.push(advertise);
        var response = {
          resCode: 200,
          response: result
        };
        //console.log(result);
        res.status(200).send(response);
      })
      // end
    }

  });
});

// line chart  to dashboard
router.get('/getChart', function (req, res, next) {
  var condiction = {};
  var result = [];
  // get
  Usermodel.getUserByMonth(condiction, function (err) {
    res.status(200).send(ERROR.COMMON.SOMETHINGWRONG);
  }, function (user) {
    if (user.length > 0) {
      result.push(user);

      // get advertise count ]
      var con = {};
      Usermodel.getRecordsByMonth(con, function (err) {
        res.status(200).send(ERROR.COMMON.SOMETHINGWRONG);
      }, function (records) {
        result.push(records);
        var response = {
          resCode: 200,
          response: result
        };
        //console.log(result);
        res.status(200).send(response);
      })
      // end
    }

  });
  // end
});

//customodule.authenticate,
router.get('/getAdvertiseById', function (req, res, next) {
  var id = req.query.id;
  console.log("Id", id);
  if (id) {
    var condiction = {
      status: '1',
      id: id,
      is_archived: 'N'
    }
  } else {
    var condiction = {
      status: '1',
      is_archived: 'N'
    }
  }
  Usermodel.getAdvertiseByCondiction(condiction, function (err) {
    res.status(200).send(ERROR.COMMON.SOMETHINGWRONG);
  }, function (advertise) {
    var response = {
      resCode: 200,
      path: customodule.setting.advertiseurl,
      response: advertise
    }
    res.status(200).send(response);
  })
});

//customodule.authenticate,
router.get('/getUserlist', function (req, res, next) {
  var user_type_id = req.query.user_type_id;
  var user_id = req.query.user_id;
  //console.log('user_id', user_id);
  if (user_type_id) {
    // AND u.status='1'
    var condiction = "u.user_type_id='" + user_type_id + "' AND u.status='1'";
  } else if (user_id) {
    // AND u.status='1'
    var condiction = "u.id ='" + user_id + "' AND u.status='1'";
  } else {
    // AND u.status='1'
    var condiction = "u.user_type_id !='4' AND u.status='1'";
  }
  var select = "u.id as userId,ifnull(u.name,'') as userName, ifnull(u.email,'') as userEmail, ifnull(u.dob,'') as userDob,ifnull(u.gender,'') as userGender,ifnull(u.phone,'') as userMobile,ifnull(u.user_type_id,'') as userTypeId,ifnull(u.user_grouptype_id,'') as userGroupId,ifnull(u.company_name,'') as companyName,ifnull(u.company_address,'') as companyAddress,ifnull(ug.group_type,'') as userGroup,ifnull(ut.type,'') as userType";
  var join = "LEFT JOIN sd_user_grouptype as ug ON ug.id=u.user_grouptype_id LEFT JOIN sd_user_type as ut ON ut.id=u.user_type_id";

  var param = {
    condiction: condiction,
    join: join,
    select: select
  }
  console.log(condiction);
  Usermodel.getUserFullInfoByCondiction(param, function (err) {
    res.status(200).send(ERROR.COMMON.SOMETHINGWRONG);
  }, function (users) {
    var response = {
      resCode: 200,
      response: users
    }
    res.status(200).send(response);
  })
});

//customodule.authenticate,
router.get('/getUserDetails', function (req, res, next) {
  var user_id = req.query.user_id;
  if (user_id) {
    // AND u.status='1'
    var condiction = "u.id ='" + user_id + "'";
  }
  var select = "u.id as userId,ifnull(u.name,'') as userName, ifnull(u.email,'') as userEmail, ifnull(u.dob,'') as userDob,ifnull(u.gender,'') as userGender,ifnull(u.phone,'') as userMobile,ifnull(u.user_type_id,'') as userTypeId,ifnull(u.user_grouptype_id,'') as userGroupId,ifnull(u.company_name,'') as companyName,ifnull(u.company_address,'') as companyAddress,ifnull(ug.group_type,'') as userGroup,ifnull(ut.type,'') as userType, ifnull(u.hobbies,'') as userHobbies, CASE  WHEN u.is_paid = 'Y' THEN 'subscribed' WHEN u.is_paid = 'N' THEN 'free' ELSE NULL END as userMode, ifnull(pu.email,'') as parentEmail, ifnull(pu.phone,'') as parentMobile";
  var join = "LEFT JOIN sd_user_grouptype as ug ON ug.id=u.user_grouptype_id LEFT JOIN sd_user_type as ut ON ut.id=u.user_type_id LEFT JOIN users as pu ON pu.id=u.refer_user_id";

  var param = {
    condiction: condiction,
    join: join,
    select: select
  }

  Usermodel.getUserDetailsByCondiction(param, function (err) {
    res.status(200).send(ERROR.COMMON.SOMETHINGWRONG);
  }, function (users) {
    console.log(users);
    var response = {
      resCode: 200,
      response: users
    }
    res.status(200).send(response);
  })
});

//customodule.authenticate,
router.post('/getUserlistAdmin', function (req, res, next) {
  var fromPageNo = req.body.fromPageNo;
  var showRecordNo = req.body.showRecordNo;
  var pageNo = (fromPageNo - 1) * showRecordNo;

  var searchValue = req.body.userTypeValue;
  var userPaid = req.body.userPaid;
  var userStatus = req.body.userStatus;
  var userPhone = req.body.phone;
  var userEmail = req.body.email;

  console.log('showRecordNo', showRecordNo);
  console.log('pageNo', pageNo);
  //console.log('searchValue', searchValue);
  console.log('phone', userPhone);
  console.log('email', userEmail);
  // console.log('userPaid', userPaid);

  // checking user paid and unpaid amount
  var is_paid = '';
  var is_paid1 = '';
  if (userPaid) {
    is_paid = "AND u.is_paid = '" + userPaid + "'";
    is_paid1 = "AND is_paid = '" + userPaid + "'";
  } else {
    is_paid = "";
    is_paid1 = "";
  }

  // checking activate & deactivate user
  var status = '';
  var status1 = '';
  if (userStatus) {
    status = " AND u.status = '" + userStatus + "'";
    status1 = " AND status = '" + userStatus + "'";
  } else {
    status = "";
    status1 = "";
  }

  // checking by email
  var email = '';
  var email1 = '';
  if (userEmail) {
    email = " AND u.email = '" + userEmail + "'";
    email1 = " AND email = '" + userEmail + "'";
  } else {
    email = "";
    email1 = "";
  }

  // checking by phone
  var phone = '';
  var phone1 = '';
  if (userPhone) {
    phone = " AND u.phone = '" + userPhone + "'";
    phone1 = " AND phone = '" + userPhone + "'";
  } else {
    phone = "";
    phone1 = "";
  }

  // checking by user type id
  var user_type_id = '';
  var user_type_id1 = '';
  if (searchValue) {
    user_type_id = " AND u.user_type_id = '" + searchValue + "'";
    user_type_id1 = " AND user_type_id = '" + searchValue + "'";
  } else {
    user_type_id = "";
    user_type_id1 = "";
  }

  var condiction = "u.user_type_id !='4'" + is_paid + "" + status + "" + email + "" + phone + "" + user_type_id;
  var condiction1 = "user_type_id !='4'" + is_paid1 + "" + status1 + "" + email1 + "" + phone1 + "" + user_type_id1;
  //
  // var condiction  = '';
  // var condiction1 = '';
  // if(searchValue){
  //   condiction    = "u.user_type_id !='4'  AND u.user_type_id='"+searchValue+"'"+is_paid+""+status;
  //   condiction1   = "user_type_id !='4'  AND user_type_id='"+searchValue+"'"+is_paid1+""+status1;
  // }  else {
  //   condiction    = "u.user_type_id !='4'"+is_paid+""+status;
  //   condiction1   = "user_type_id !='4'"+is_paid1+""+status1;
  // }

  //console.log(condiction);

  var select = "u.id as userId,ifnull(u.name,'') as userName, ifnull(u.email,'') as userEmail, ifnull(u.dob,'') as userDob,ifnull(u.gender,'') as userGender,ifnull(u.phone,'') as userMobile,ifnull(u.user_type_id,'') as userTypeId,ifnull(u.user_grouptype_id,'') as userGroupId,ifnull(u.company_name,'') as companyName,ifnull(u.company_address,'') as companyAddress,ifnull(ug.group_type,'') as userGroup,ifnull(ut.type,'') as userType, (select count(id) from users where " + condiction1 + ") AS totalCnt, CASE  WHEN u.is_paid = 'Y' THEN 'Subscribed' WHEN u.is_paid = 'N' THEN 'Free' ELSE NULL END as userMode, CASE  WHEN u.status = 1 THEN 'Activate' WHEN u.status = 0 THEN 'Deactivate' ELSE NULL END as userStatus";
  var join = "LEFT JOIN sd_user_grouptype as ug ON ug.id=u.user_grouptype_id LEFT JOIN sd_user_type as ut ON ut.id=u.user_type_id";
  var limit = "LIMIT " + pageNo + "," + showRecordNo;

  var param = {
    condiction: condiction,
    join: join,
    select: select,
    limit: limit
  };

  //console.log(param);
  Usermodel.getUserFullInfoByCondiction(param, function (err) {
    res.status(200).send(ERROR.COMMON.SOMETHINGWRONG);
  }, function (users) {
    var response = {
      resCode: 200,
      response: users,
      fromRecordNo: fromPageNo,
    }
    res.status(200).send(response);
  })
});
//customodule.authenticate,
router.post('/updateUserAdmin', function (req, res, next) {

  var id = req.body.userId;
  var name = req.body.userName;
  var gender = req.body.userGender;
  var hobbies = req.body.userHobbies;
  var phone = req.body.userMobile;
  var company_name = req.body.companyName;
  var company_address = req.body.compAddress;
  if (!id) {
    res.status(200).send(ERROR.DIARY.REQUIREDFIELD);
  } else {
    var data = {
      name: name,
      gender: gender,
      hobbies: hobbies,
      phone: phone,
      company_name: company_name,
      company_address: company_address
    }
    var condiction = {
      id: id
      //status:'1'
    }
    Usermodel.updateUserByCondiction(data, condiction, function (err) { res.status(200).send(ERROR.COMMON.SOMETHINGWRONG); }, function (success) {
      if (success) {
        var response = {
          resCode: 200,
          response: "Profile updated successfully"
        }
        res.status(200).send(response);
      }
    })
  }
})
router.put('/updateUser', function (req, res, next) {
  var id = req.body.userId;
  var name = req.body.userName;
  var gender = req.body.userGender;
  var dob = req.body.userDob;
  var phone = req.body.userPhone;
  var company_name = req.body.userCompanyName;
  var company_address = req.body.userCompanyAddress;
  if (!id) {
    res.status(200).send(ERROR.DIARY.REQUIREDFIELD);
  } else {
    var data = {
      name: name,
      gender: gender,
      dob: dob,
      phone: phone,
      company_name: company_name,
      company_address: company_address
    }
    var condiction = {
      id: id
      //status:'1'
    }
    Usermodel.updateUserByCondiction(data, condiction, function (err) { res.status(200).send(ERROR.COMMON.SOMETHINGWRONG); }, function (success) {
      if (success) {
        var response = {
          resCode: 200,
          response: "Profile updated successfully"
        }
        res.status(200).send(response);
      }
    })
  }
})
router.post('/deleteUser', function (req, res, next) {
  var id = req.body.userId;
  var userStatus = req.body.userStatus;

  if (!id) {
    res.status(200).send(ERROR.DIARY.REQUIREDFIELD);
  } else {
    var status = '';
    if (userStatus == 'Activate') {
      status = 0;
    } else if (userStatus == 'Deactivate') {
      status = 1;
    }
    var data = {
      status: status
    }
    var condiction = {
      id: id
    }
    Usermodel.updateUserByCondiction(data, condiction, function (err) { res.status(200).send(ERROR.COMMON.SOMETHINGWRONG); }, function (success) {
      if (success) {
        var response = {
          resCode: 200,
          response: "Profile updated successfully"
        }
        res.status(200).send(response);
      }
    })
  }
})

router.post('/deleteUserById', function (req, res, next) {
  var id = req.body.userId;
  if (!id) {
    res.status(200).send(ERROR.DIARY.REQUIREDFIELD);
  } else {
    var condiction = {
      id: id
    }
    Usermodel.deleteUserByCondiction(condiction, function (err) { res.status(200).send(ERROR.COMMON.SOMETHINGWRONG); }, function (success) {
      //console.log('success', success);
      if (success == true) {
        var response = {
          resCode: 200,
          response: "User deleted successfully"
        }
        res.status(200).send(response);
      }
    })
  }
})

//customodule.authenticate,
router.post('/addCategory', function (req, res, next) {
  var category_name = req.body.cat_name;
  var category_name_ar = req.body.cat_name_ar;
  var category_name_ch = req.body.cat_name_ch;
  var category_name_hn = req.body.cat_name_hn;
  var category_name_tg = req.body.cat_name_tg;
  if (!category_name || !category_name_ar || !category_name_ch || !category_name_hn || !category_name_tg) {
    res.status(200).send(ERROR.DIARY.REQUIREDFIELD);
  } else {
    var condiction = {
      category_name: category_name,
      category_name_ar: category_name_ar,
      category_name_ch: category_name_ch,
      category_name_hindi: category_name_hn,
      category_name_tg: category_name_tg
    }
    Usermodel.getCategoryByCondiction(condiction, function (err) { res.status(200).send(ERROR.COMMON.SOMETHINGWRONG); }, function (response) {
      if (response.length > 0) {
        res.status(200).send(ERROR.CATEGORY.ALREADYEXIST);
      } else {
        condiction.category_name_ar = category_name_ar;
        Usermodel.addCategory(condiction, function (err) { res.status(200).send(ERROR.COMMON.SOMETHINGWRONG); }, function (lastid) {
          if (lastid) {
            var response = {
              resCode: 200,
              response: "Category created successfully"
            }
            res.status(200).send(response);
          } else {
            res.status(200).send(ERROR.COMMON.SOMETHINGWRONG);
          }
        })
      }
    })
  }
})
//customodule.authenticate,
router.get('/getCategory', function (req, res, next) {
  var id = req.query.cat_id;
  if (id) {
    var condiction = {
      id: id
    }
  } else {
    var condiction = {
      'id !=': ''
    }
  }
  Usermodel.getCategoryByIdCondiction(condiction, function (err) {
    res.status(200).send(ERROR.COMMON.SOMETHINGWRONG);
  }, function (category) {
    var response = {
      resCode: 200,
      response: category
    }
    res.status(200).send(response);
  })
});
//customodule.authenticate,
router.put('/updateCategory', function (req, res, next) {
  var category_name = req.body.cat_name;
  var category_name_ar = req.body.cat_name_ar;
  var category_name_ch = req.body.cat_name_ch;
  var category_name_hn = req.body.cat_name_hn;
  var category_name_tg = req.body.cat_name_tg;
  var id = req.body.cat_id;
  console.log(req.body);
  if (!category_name || !id || !category_name_ar || !category_name_ch || !category_name_hn || !category_name_tg) {
    res.status(200).send(ERROR.DIARY.REQUIREDFIELD);
  } else {
    var condiction = {
      category_name: category_name,
      category_name_ar: category_name_ar,
      category_name_ch: category_name_ch,
      category_name_hindi: category_name_hn,
      category_name_tg: category_name_tg,
      id: id
    }

    Usermodel.getCategoryByCondictionEdit(condiction, function (err) { res.status(200).send(ERROR.COMMON.SOMETHINGWRONG); }, function (response) {
      if (response.length > 0) {
        res.status(200).send(ERROR.CATEGORY.ALREADYEXIST);
      } else {
        var cond = {
          id: id
        };
        var data = {
          category_name: category_name,
          category_name_ar: category_name_ar,
          category_name_ch: category_name_ch,
          category_name_hindi: category_name_hn,
          category_name_tg: category_name_tg
        }
        Usermodel.updateCategory(data, cond, function (err) { res.status(200).send(ERROR.COMMON.SOMETHINGWRONG); }, function (update) {
          if (update) {
            var response = {
              resCode: 200,
              response: "Category updated successfully"
            }
            res.status(200).send(response);
          } else {
            res.status(200).send(ERROR.COMMON.SOMETHINGWRONG);
          }
        })
      }
    })
  }
})

//customodule.authenticate,

router.get('/deleteCategory', function (req, res, next) {
  var id = req.query.id;
  var condiction = {
    id: id
  }
  if (!id) {
    //  res.status(200).send(ERROR.DIARY.REQUIREDFIELD);
  } else {
    var cond = {
      category_id: id
    }
    Usermodel.getRecordByCondiction(cond, function (err) {
      res.status(200).send(ERROR.COMMON.SOMETHINGWRONG);
    }, function (response) {
      console.log(2);
      if (response.length > 0) {
        res.status(200).send(ERROR.CATEGORY.BELONGSTO);
      } else {
        Usermodel.deleteCategory(condiction, function (err) {
          res.status(200).send(ERROR.COMMON.SOMETHINGWRONG);
        }, function (updateshare) {
          var response = {
            resCode: 200,
            response: "Category has been deleted successfully"
          }
          res.status(200).send(response);
        })
      }
    })
  }
});
//customodule.authenticate,
router.get('/getAdvertisePaymentDetails', function (req, res, next) {
  // var user_id       = req.query.user_id;
  // if(user_id){
  //   var condiction="u.id ='"+user_id+"' AND u.status='1'";
  // }
  var select = "u.id as userId,ifnull(u.name,'') as userName, ifnull(u.email,'') as userEmail, ifnull(u.dob,'') as userDob,ifnull(u.gender,'') as userGender,ifnull(u.phone,'') as userMobile,ifnull(u.user_type_id,'') as userTypeId,ifnull(u.user_grouptype_id,'') as userGroupId,ifnull(u.company_name,'') as companyName,ifnull(u.company_address,'') as companyAddress,ifnull(p.txnid,'') as txnid,ifnull(p.payment_status,'') as paymentStatus, ifnull(p.payment_amount,'') as paymentAmount, ifnull(p.createdtime,'') as createdtime, ifnull(adv.advt_name,'') as advName";
  var join = "LEFT JOIN users as u ON u.id=p.userid LEFT JOIN sd_advertisements as adv ON p.itemid=adv.id";

  var param = {
    //condiction: condiction,
    join: join,
    select: select
  };
  Usermodel.getadvPaymentDetailsByCondiction(param, function (err) {
    res.status(200).send(ERROR.COMMON.SOMETHINGWRONG);
  }, function (users) {
    //console.log(users);
    var response = {
      resCode: 200,
      response: users
    }
    res.status(200).send(response);
  })
});

//customodule.authenticate,
router.get('/getUserPaymentDetails', function (req, res, next) {
  // var user_id       = req.query.user_id;
  // if(user_id){
  //   var condiction="u.id ='"+user_id+"' AND u.status='1'";
  // }
  var select = "u.id as userId,ifnull(u.name,'') as userName, ifnull(u.email,'') as userEmail, ifnull(u.dob,'') as userDob,ifnull(u.gender,'') as userGender,ifnull(u.phone,'') as userMobile,ifnull(u.user_type_id,'') as userTypeId,ifnull(u.user_grouptype_id,'') as userGroupId,ifnull(u.company_name,'') as companyName,ifnull(u.company_address,'') as companyAddress,ifnull(up.transactionsAmt,'') as transactionsAmt,ifnull(up.transactionsId,'') as transactionsId, ifnull(up.transactionsDate,'') as transactionsDate, ifnull(up.invoiceNumber,'') as invoiceNumber";
  var join = "LEFT JOIN users as u ON u.id=up.user_id";

  var param = {
    //condiction: condiction,
    join: join,
    select: select
  }

  Usermodel.getuserPaymentDetailsByCondiction(param, function (err) {
    res.status(200).send(ERROR.COMMON.SOMETHINGWRONG);
  }, function (users) {
    console.log(users);
    var response = {
      resCode: 200,
      response: users
    }
    res.status(200).send(response);
  })
});

//customodule.authenticate,
router.post('/getUserPaymentlist', function (req, res, next) {
  var paymentUser = req.body.paymentUser;
  var fromPageNo = req.body.fromPageNo;
  var showRecordNo = req.body.showRecordNo;
  var pageNo = (fromPageNo - 1) * showRecordNo;

  if (paymentUser == 'advertisementUser') {
    var select = "p.txnid as txnId,ifnull(p.payment_amount,'') as paymentAmt, ifnull(p.payment_status,'') as paymentStatus, ifnull(u.name,'') as userName,ifnull(adv.advt_name,'') as advtName, ifnull(p.createdtime,'') as createdtime, (select count(id) from payments) AS totalCnt";
    var join = "LEFT JOIN users as u ON u.id=p.userid LEFT JOIN sd_advertisements as adv ON adv.id=p.itemid";
    var limit = "LIMIT " + pageNo + "," + showRecordNo;

    var param = {
      join: join,
      select: select,
      limit: limit
    };

    Usermodel.getAdvUserPaymentByCondiction(param, function (err) {
      res.status(200).send(ERROR.COMMON.SOMETHINGWRONG);
    }, function (users) {
      var response = {
        resCode: 200,
        response: users
      }
      res.status(200).send(response);
    })
  }
});


router.post('/getSubscribeUserPaymentlist', function (req, res, next) {
  var paymentUser = req.body.paymentUser;
  var fromPageNo = req.body.fromPageNo;
  var showRecordNo = req.body.showRecordNo;
  var pageNo = (fromPageNo - 1) * showRecordNo;

  if (paymentUser == 'subscribeUser') {
    var select = "p.transactionsId as transactionsId,ifnull(p.transactionsDate,'') as transactionsDate, ifnull(u.name,'') as userName, (select count(id) from sd_user_payment_info) AS totalCnt";
    var join = "LEFT JOIN users as u ON u.id=p.user_id";
    var limit = "LIMIT " + pageNo + "," + showRecordNo;

    var param = {
      join: join,
      select: select,
      limit: limit
    };

    Usermodel.getSubscribeUserPaymentByCondiction(param, function (err) {
      res.status(200).send(ERROR.COMMON.SOMETHINGWRONG);
    }, function (users) {
      var response = {
        resCode: 200,
        response: users
      }
      res.status(200).send(response);
    })
  }
});




router.post('/getRecord', function (req, res, next) {
  var fromPageNo = req.body.fromPageNo;
  var showRecordNo = req.body.showRecordNo;
  var pageNo = (fromPageNo - 1) * showRecordNo;


  var select = "r.title as title,ifnull(r.description,'') as description, ifnull(r.post_date,'') as post_date, ifnull(r.awardType,'') as awardType, concat('" + customodule.setting.diaryurl + "',CASE WHEN r.drawing='' THEN 'no-img.jpg' ELSE r.drawing END) as recordImage, ifnull(u.name,'') as userName,ifnull(c.category_name,'') as category_name, (select count(id) from sd_records where status = 1) AS totalCnt";
  var join = "LEFT JOIN users as u ON u.id=r.user_id LEFT JOIN sd_master_categories as c ON c.id=r.category_id";
  var limit = "LIMIT " + pageNo + "," + showRecordNo;
  var condiction = "r.status = 1";

  var param = {
    condiction: condiction,
    join: join,
    select: select,
    limit: limit
  };
  //console.log(param);
  Usermodel.getRecordListByCondiction(param, function (err) {
    res.status(200).send(ERROR.COMMON.SOMETHINGWRONG);
  }, function (users) {
    var response = {
      resCode: 200,
      response: users
    }
    res.status(200).send(response);
  })

});

module.exports = router;
