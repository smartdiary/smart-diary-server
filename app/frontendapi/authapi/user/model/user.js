var database = require('../../../../../config/database');
var qb = require('node-querybuilder').QueryBuilder(database, 'mysql', 'single');
var model = {
  getUserByCondiction: function (condiction, errorcallback, successcallback) {
    return qb.select('*')
      .where(condiction)
      .get('users', function (err, response) {
        if (err) errorcallback("Uh oh! Couldn't get results: " + err.msg);
        successcallback(response);
      });
  },
  updateUserByCondiction: function (data, condiction, errorcallback, successcallback) {
    return qb.update('users', data, condiction, function (err, res) {
      if (err) errorcallback("Uh oh! Couldn't update results: " + err.msg);
      successcallback(true);
    });
  },
  getUserFullInfoByCondiction: function (param, errorcallback, successcallback) {
    // var qu = "Select "+param.select+" from users as u "+param.join+" where "+param.condiction+" "+param.limit;
    // console.log(qu);
    return qb.query("Select " + param.select + " from users as u " + param.join + " where " + param.condiction + " " + param.limit, function (err, response) {
      successcallback(response);
    })
  },
  getAdvUserPaymentByCondiction: function (param, errorcallback, successcallback) {
    return qb.query("Select " + param.select + " from payments as p " + param.join + " " + param.limit, function (err, response) {
      successcallback(response);
    })
  },
  getSubscribeUserPaymentByCondiction: function (param, errorcallback, successcallback) {
    return qb.query("Select " + param.select + " from sd_user_payment_info as p " + param.join + " " + param.limit, function (err, response) {
      successcallback(response);
    })
  },
  getRecordListByCondiction: function (param, errorcallback, successcallback) {
    var qq = "Select " + param.select + " from sd_records as r " + param.join + " where " + param.condiction + " " + param.limit;
    console.log('check', qq);
    return qb.query("Select " + param.select + " from sd_records as r " + param.join + " where " + param.condiction + " " + param.limit, function (err, response) {
      successcallback(response);
    })
  },
  deleteUserByCondiction: function (param, errorcallback, successcallback) {
    console.log(param.id);
    var sql = "DELETE FROM users WHERE id=" + param.id;
    return qb.query(sql, function (err, response) {
      successcallback(true);
    });
  },

  getUserByMonth: function (param, errorcallback, successcallback) {
    var query = "SELECT YEAR(created) AS y, MONTH(created) AS m, COUNT(DISTINCT id) as userId FROM users where status= 1 AND user_type_id != 4 GROUP BY y, m";
    return qb.query(query, function (err, response) {
      successcallback(response);
    });
  },

  getRecordsByMonth: function (param, errorcallback, successcallback) {
    var query = "SELECT YEAR(created) AS y, MONTH(created) AS m, COUNT(DISTINCT id) as recordId FROM sd_records where status= 1 GROUP BY y, m";
    return qb.query(query, function (err, response) {
      successcallback(response);
    });
  },

  getUserDetailsByCondiction: function (param, errorcallback, successcallback) {
    //var qu = "Select "+param.select+" from users as u "+param.join+" where "+param.condiction;
    return qb.query("Select " + param.select + " from users as u " + param.join + " where " + param.condiction, function (err, response) {
      successcallback(response);
    })
  },
  getAccessTokenByCondiction: function (condiction, errorcallback, successcallback) {
    return qb.select('*')
      .where(condiction)
      .get('sd_access_token', function (err, response) {
        if (err) errorcallback("Uh oh! Couldn't get results: " + err.msg);
        successcallback(response);
      });
  },
  addAdvertise: function (data, errorcallback, successcallback) {
    return qb.insert('sd_advertisements', data, function (err, res) {
      //  qb.release();
      if (err) errorcallback(err);
      successcallback(res.insertId);
    });
  },
  updateAdvertise: function (data, condiction, errorcallback, successcallback) {
    return qb.update('sd_advertisements', data, condiction, function (err, res) {
      //console.log('err',err);
      if (err) errorcallback("Uh oh! Couldn't update results: " + err.msg);
      successcallback(true);
    });
  },
  getAdvertiseByCondiction: function (condiction, errorcallback, successcallback) {
    return qb.select('*')
      .where(condiction)
      .get('sd_advertisements', function (err, response) {
        if (err) errorcallback("Uh oh! Couldn't get results: " + err.msg);
        successcallback(response);
      });
  },
  getAdvertiseTypeByCondiction: function (condiction, errorcallback, successcallback) {
    var query = '';
    if (condiction.user_id) {
      query = "select (select count(*) from sd_advertisements where status= 1 and is_archived = 'N' and user_id=" + condiction.user_id + ") as totalAdv," +
        "(select count(*) from sd_advertisements where status= 1 and is_archived = 'N' and is_payment_set = 'N' and user_id=" + condiction.user_id + ") as setPayment," +
        "(select count(*) from sd_advertisements where status= 1 and is_archived = 'N' and is_paid = 'N' and is_payment_set = 'Y' and user_id=" + condiction.user_id + ") as receivedPayment," +
        "(select count(*) from sd_advertisements where status= 1 and is_archived = 'N' and is_published = 'N' and is_paid = 'Y' and user_id=" + condiction.user_id + ") as activate, " +
        "(select count(*) from sd_advertisements where status= 1 and is_archived = 'N' and published_date + INTERVAL 30 DAY <= CURDATE() and user_id=" + condiction.user_id + ") as previous " +
        "from sd_advertisements";
    } else {
      query = "select (select count(*) from sd_advertisements where status= 1 and is_archived = 'N') as totalAdv," +
        "(select count(*) from sd_advertisements where status= 1 and is_archived = 'N' and is_payment_set = 'N') as setPayment," +
        "(select count(*) from sd_advertisements where status= 1 and is_archived = 'N' and is_paid = 'N' and is_payment_set = 'Y') as receivedPayment," +
        "(select count(*) from sd_advertisements where status= 1 and is_archived = 'N' and is_published = 'N' and is_paid = 'Y') as activate, " +
        "(select count(*) from sd_advertisements where status= 1 and is_archived = 'N' and published_date + INTERVAL 30 DAY <= CURDATE()) as previous " +
        "from sd_advertisements";
    }

    return qb.query(query, function (err, response) {
      successcallback(response);
    })
  },
  getUserTypeByCondiction: function (condiction, errorcallback, successcallback) {
    var query = "select (select count(*) from users where status= 1 AND user_type_id != 4) as activateUser," +
      "(select count(*) from users where status= 0 AND user_type_id != 4) as deactivateUser," +
      "(select count(*) from users where is_paid = 'N' AND user_type_id != 4) as freeUser," +
      "(select count(*) from users where is_paid = 'Y' AND user_type_id != 4) as subscribeUser," +
      "(select count(*) from users where user_type_id != 4) as totalUser " +
      "from users";
    return qb.query(query, function (err, response) {
      successcallback(response);
    })
  },
  getCategoryByCondiction: function (condiction, errorcallback, successcallback) {
    console.log('condiction', condiction);
    var sql = "Select * from sd_master_categories WHERE category_name ='" + condiction.category_name + "' && category_name_ar='" + condiction.category_name_ar + "' && category_name_ch='" + condiction.category_name_ch + "' && category_name_hindi='" + condiction.category_name_hindi + "' && category_name_tg='" + condiction.category_name_tg + "'";
    console.log(sql);
    return qb.query(sql, function (err, response) {
      successcallback(response);
    })
  },
  getCategoryByCondictionEdit: function (condiction, errorcallback, successcallback) {
    console.log('condiction', condiction);
    var sql = "Select * from sd_master_categories WHERE (category_name ='" + condiction.category_name + "' OR category_name_ar='" + condiction.category_name_ar + "' OR category_name_ch='" + condiction.category_name_ch + "' OR category_name_hindi='" + condiction.category_name_hindi + "' OR category_name_tg='" + condiction.category_name_tg + "' ) AND id !=  " + condiction.id;
    console.log(sql);
    return qb.query(sql, function (err, response) {
      successcallback(response);
    })
  },
  getCategoryByIdCondiction: function (condiction, errorcallback, successcallback) {
    return qb.select('*')
      .where(condiction)
      .get('sd_master_categories', function (err, response) {
        if (err) errorcallback("Uh oh! Couldn't get results: " + err.msg);
        successcallback(response);
      });
  },
  addCategory: function (data, errorcallback, successcallback) {
    return qb.insert('sd_master_categories', data, function (err, res) {
      //  qb.release();
      console.log("err", err);
      console.log("res", res);
      if (err) errorcallback(err);
      successcallback(res.insertId);
    });
  },

  updateCategory: function (data, condiction, errorcallback, successcallback) {
    return qb.update('sd_master_categories', data, condiction, function (err, res) {
      console.log(err);
      if (err) errorcallback("Uh oh! Couldn't update results: " + err.msg);
      successcallback(true);
    });
  },
  deleteCategory: function (cond, errorcallback, successcallback) {
    qb.delete('sd_master_categories', cond, function (err, res) {
      console.log(err);
      if (err) errorcallback("Uh oh! Couldn't delete results: " + err);
      successcallback(true);
    });
  },
  getRecordByCondiction: function (condiction, errorcallback, successcallback) {
    return qb.select('*')
      .where(condiction)
      .get('sd_records', function (err, response) {
        if (err) errorcallback("Uh oh! Couldn't get results: " + err.msg);
        successcallback(response);
      });
  },
  getadvPaymentDetailsByCondiction: function (param, errorcallback, successcallback) {
    return qb.query("Select " + param.select + " from payments as p " + param.join, function (err, response) {
      successcallback(response);
    })
  },
  getuserPaymentDetailsByCondiction: function (param, errorcallback, successcallback) {
    return qb.query("Select " + param.select + " from sd_user_payment_info as up " + param.join, function (err, response) {
      successcallback(response);
    })
  },
}
module.exports = model;
