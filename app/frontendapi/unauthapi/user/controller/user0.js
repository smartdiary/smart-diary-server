var express             = require('express');
var router              = express.Router();
var app                 = express();
var customodule         = require('../../../../customexport');
var node_module         = require('../../../../export');
var ERROR               = require('../../../../../config/statuscode');
var Usermodel           = require('../model/user');

// router.get('/getUsertype',customodule.validateapi,function(req,res,next){
//   Usermodel.getUserType(function(err){console.log()},function(response){
//     var response={
//         success     : true,
//         resCode     : 200,
//         resMessage  : response
//     };
//     res.status(200).send(response);
//   });
// });

// router.get('/getGrouptype',customodule.validateapi,function(req,res,next){
//   Usermodel.getGrouptype(function(err){console.log()},function(response){
//     var response={
//         success     : true,
//         resCode     : 200,
//         resMessage  : response
//     };
//     res.status(200).send(response);
//   });
// });

// router.post('/emailVarify',customodule.validateapi,function(req,res,next){
//   var email = req.body.email;
//   if(!email){
//     res.status(200).json(ERROR.USER.EMAILREQUIRED);
//   }else{
//         var generatedotp=Math.floor((Math.random() * 10000000) + 1);
//         var response={
//             success     : true,
//             resCode     : 200,
//             response  : generatedotp
//         };
//         customodule.mail(email,generatedotp,'emailverify');
//         res.status(200).send(response)
//   }
// });

router.post('/childRegistration',customodule.validateapi,function(req,res,next){

  var name            = req.body.userName;
  var email           = req.body.userEmail;
  var dob             = req.body.userDob;
  var gender          = req.body.userGender;
  var mobile          = req.body.userMobile;
  var parent_email    = req.body.parentEmail;
  var parent_mobile   = req.body.parentMobile;
  var password        = req.body.userPassword;
  var user_grouptype  = req.body.userGroup;
  var source          = 'App';
  var response  = {
      success:true,
      resCode:200,
      response:"You have successfully registered . Please check your mail for account verification."
  }
  if(!email || !gender || !name || !dob || !password){
    res.status(200).send(ERROR.USER.REQUIREDFIELD);
  }else{
    var useremailcond  = {
          email:email
       };
    Usermodel.getUserByCondiction(useremailcond,function(err){console.log("Error",err)},function(userlist){
    //  console.log(userlist);
      if(userlist.length>0){
        res.status(200).send(ERROR.USER.USERALREADYEXIST);
      }else{
        var password  = node_module.crypto.createHash('md5').update(req.body.userPassword).digest("hex");
        var grouptypecond = {
              group_type:user_grouptype
          }
        Usermodel.getGrouptypeByCondiction(grouptypecond,function(err){console.log(err)},function(usergroup){
          if(usergroup.length>0){
            var user_grouptype_id=usergroup[0].id;
            var user_age  = customodule.helper.getAgeUsingDob(dob).split('-');
            if(user_age[0]>19){
              res.status(200).send(ERROR.USER.INVALIDAGE);
            }else if(user_age[0]<=11 && user_age[0]>4){
              if(!parent_email || !parent_mobile){
                res.status(200).send(ERROR.USER.EMAILPHONE);
              }else{
                if(parent_email==email || parent_mobile==mobile){
                  res.status(200).send(ERROR.USER.DUPLICATEENTRY)
                }else{
                  var parentcond = {
                    email:parent_email
                    }
                  Usermodel.getUserByCondiction(parentcond,function(err){console.log(err)},function(parentlist){
                    if(parentlist.length>0){
                        res.status(200).send(ERROR.USER.PARENTALREADYEXIST);
                    }else{
                      var parentdata={
                        email       : parent_email,
                        phone       : parent_mobile,
                        password    : password,
                        user_type_id: 2,
                      }
                      /* Send Mail to parent  and insert parent data to database*/
                      Usermodel.addUser(parentdata,function(err){
                        console.log("Error:: ",err);
                      },function(ref_id){
                        var parent_verification_token = node_module.jwt.sign(parentdata, customodule.setting.jwtsecret);
                        customodule.mail(parent_email,parent_verification_token,'parent');
                        var childdata={
                              name    : name,
                              email   : email,
                              dob     : dob,
                              gender  : gender,
                              phone   : mobile,
                              user_type_id      : 1,
                              user_grouptype_id : user_grouptype_id,
                              password          : password,
                              refer_user_id     : ref_id
                        }
                        /* Send Mail to child  and insert child data to database*/
                        Usermodel.addUser(childdata,function(err){
                          console.log("Error::",err)
                        },function(success){
                          var verification_token = node_module.jwt.sign(childdata, customodule.setting.jwtsecret);
                          customodule.mail(email,verification_token,'child');
                          res.status(200).send(response);
                        })
                      })
                    }
                  })
                }
              }
            }else if (user_age[0]<5) {
              res.status(200).send(ERROR.USER.LESSAGE);
            }else if(user_age[0] > 11 && user_age[0]<=19){
              var childdata={
                    name    : name,
                    email   : email,
                    dob     : dob,
                    gender  : gender,
                    phone   : mobile,
                    user_type_id      : 2,
                    user_grouptype_id : user_grouptype_id,
                    password          : password,
              }
              /* Send Mail to child  and insert child  data to database*/
              Usermodel.addUser(childdata,function(err){
                console.log("Error::",err)
              },function(success){
                var verification_token = node_module.jwt.sign(childdata, customodule.setting.jwtsecret);
                customodule.mail(email,verification_token,'child');
                res.status(200).send(response);
              })
            }
          }else{
            res.status(200).send(ERROR.USER.INVALIDGROUPTYPE);
          }
        });
      }
    });
  }
});

router.get('/emailverify',function(req,res,next){
  var token_data  = req.query.token;
  var usercond =  {
    verification_token:token_data
  }
  console.log(usercond);
  Usermodel.getUserByCondiction(usercond,function(err){console.log(err)},function(users){
    if(users.length>0){
      var data ={
        status:1,
        verification_token:''
      }
      Usermodel.updateUserByCondiction(data,usercond,function(err){console.log(err)},function(success){
        res.sendFile(node_module.path.join(__dirname+'/../../../../../views/success.html'));
      })
    }else{
        res.sendFile(node_module.path.join(__dirname+'/../../../../../views/failure.html'));
    }
  })
});
router.post('/childLogin',customodule.validateapi,function(req,res,next){
  var email         = req.body.email;
  var password      = node_module.crypto.createHash('md5').update(req.body.password).digest("hex");
  var user_type_id  = req.body.user_type_id;
  var condiction    = {
                        email       : email,
                        password    : password,
                        user_type_id: user_type_id
                      }
  Usermodel.getUserByCondiction(condiction,function(err){console.log("Error in login::",err)},function(user){
    if(users.length>0){
      var access_token = node_module.jwt.sign(user[0], customodule.setting.jwtsecret);
      /* Remove previous access_token and created a new one */
      Usermodel.removeAccessToken(user[0].id,function(err){},function(success){
        var token_data   = {
                              user_id : user[0].id,
                              token   : access_token
                           }
        Usermodel.createAccessToken(token_data,function(err){},function(success){
          Usermodel.getUserInfo()
        })
      })
    }else{
      res.status(200).send(ERROR.USER.INVALIDLOGINCREDENTIAL);
    }
  })

})

module.exports = router;
