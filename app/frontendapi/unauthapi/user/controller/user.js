var express             = require('express');
var router              = express.Router();
var app                 = express();
var customodule         = require('../../../../customexport');
var node_module         = require('../../../../export');
var ERROR               = require('../../../../../config/statuscode');
var Usermodel           = require('../model/user');


router.post('/advertiserRegistration',customodule.validateapi,function(req,res,next){
  var name            = req.body.userName;
  var email           = req.body.userEmail;
  var mobile          = req.body.userMobile;
  var password        = req.body.userPassword;
  var company_name    = req.body.companyName;
  var company_address = req.body.companyAddress;
  var user_type       = req.body.userType;

  var response  = {
      resCode:200,
      response:"You have successfully registered . Please check your mail for account verification."
  }
  if(!email || !name || !password || !mobile || !user_type){
    res.status(200).send(ERROR.USER.REQUIREDFIELD);
  }else{
    var usertypecond  = {
      type:user_type
    }
    Usermodel.getUserTypeByCondiction(usertypecond,function(err){console.log(err)},function(usertype){
      if(usertype.length>0){
        var usertype_id = usertype[0].id;
        var useremailcond  = {
              email:email
           };
           Usermodel.getUserByCondiction(useremailcond,function(err){console.log("Error",err)},function(userlist){
             if(userlist.length>0){
               res.status(200).send(ERROR.USER.USERALREADYEXIST);
             }else{
               var password  = node_module.crypto.createHash('md5').update(customodule.setting.passprefix+req.body.userPassword).digest("hex");
               var verification_token = node_module.jwt.sign({time:new Date().getTime()}, customodule.setting.jwtsecret);
                     var userdata={
                           name    : name,
                           email   : email,
                           phone   : mobile,
                           user_type_id      : usertype_id,
                           password          : password,
                           company_name      : company_name,
                           company_address   : company_address,
                           verification_token: verification_token
                     }
                     /* Send Mail to child  and insert child  data to database*/
                     Usermodel.addUser(userdata,function(err){
                       console.log("Error::",err)
                     },function(success){
                       customodule.mail(email,verification_token,'frontuser','');
                       res.status(200).send(response);
                     })
             }
           });
      }else{
        res.status(200).send(ERROR.USER.INVALIDUSERTYPE);
      }
    })
  }
});

router.post('/login',customodule.validateapi,function(req,res,next){
  var email         = req.body.userEmail;
  var pwd           = customodule.setting.passprefix+req.body.userPassword;
  var password      = node_module.crypto.createHash('md5').update(pwd).digest("hex");
  var condiction="u.email='"+email+"' AND u.password='"+password+"' AND u.status='1'";
  var select  = "u.id as userId,ifnull(u.name,'') as userName, ifnull(u.email,'') as userEmail, ifnull(u.dob,'') as userDob,ifnull(u.gender,'') as userGender,ifnull(u.phone,'') as userMobile,ifnull(u.user_type_id,'') as userTypeId,ifnull(u.user_grouptype_id,'') as userGroupId,ifnull(u.company_name,'') as companyName,ifnull(u.company_address,'') as companyAddress,ifnull(ug.group_type,'') as userGroup,ifnull(ut.type,'') as userType";
  var join    = "LEFT JOIN sd_user_grouptype as ug ON ug.id=u.user_grouptype_id LEFT JOIN sd_user_type as ut ON ut.id=u.user_type_id";
  var param     = {
    condiction: condiction,
    join      : join,
    select    : select
  }
  if(!req.body.userEmail || !req.body.userPassword){
    res.status(200).send(ERROR.USER.REQUIREDFIELD);
  }else{
    Usermodel.getUserFullInfoByCondiction(param,function(err){console.log("Error in login::",err)},function(user){
      if(user.length>0){
        var access_token = node_module.jwt.sign({time:new Date().getTime()}, customodule.setting.jwtsecret);
        /* Remove previous access_token and created a new one */
        console.log(user)
        var tokencond={
          user_id:user[0].userId
        }
        Usermodel.removeAccessToken(tokencond,function(err){},function(success){
          var token_data   = {
                                user_id : user[0].userId,
                                token   : access_token
                             }
          console.log(token_data);
          Usermodel.createAccessToken(token_data,function(err){},function(success){
            var response  = {
                resCode:200,
                token:access_token,
                response:user[0]
            }
            res.status(200).send(response);
          })
        })
      }else{
        res.status(200).send(ERROR.USER.INVALIDLOGINCREDENTIAL);
      }
    })
  }
})

module.exports = router;
