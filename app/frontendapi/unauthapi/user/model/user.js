var database          = require('../../../../../config/database');
var qb = require('node-querybuilder').QueryBuilder(database, 'mysql', 'single');
var model = {
  getUserTypeByCondiction :function(condiction,errorcallback,successcallback){
    return qb.select('*')
      .where(condiction)
      .get('sd_user_type', function(err,response) {
          if (err) errorcallback("Uh oh! Couldn't get results: " + err.msg);
          successcallback(response);
      });
  },
  getGrouptypeByCondiction :function(condiction,errorcallback,successcallback){
    return qb.select('*')
      .where(condiction)
      .get('sd_user_grouptype', function(err,response) {
          if (err) errorcallback("Uh oh! Couldn't get results: " + err.msg);
          successcallback(response);
      });
  },
  addUser :function(data,errorcallback,successcallback){
    return qb.insert('users', data, function(err, res) {
          //  qb.release();
            if (err) errorcallback(err);
            successcallback(res.insertId);
        });
  },
  getUserByCondiction :function(condiction,errorcallback,successcallback){
    return qb.select('*')
      .where(condiction)
      .get('users', function(err,response) {
          if (err) errorcallback("Uh oh! Couldn't get results: " + err.msg);
          successcallback(response);
      });
  },
  updateUserByCondiction :function(data,condiction,errorcallback,successcallback){
    return qb.update('users', data, condiction ,function(err, res) {
      if (err) errorcallback("Uh oh! Couldn't update results: " + err.msg);
      successcallback(true);
    });
  },
 getUserFullInfoByCondiction :function(param,errorcallback,successcallback){
   return qb.query("Select "+param.select+" from users as u "+param.join+" where "+param.condiction,function(err,response){
      successcallback(response);
    })
 },
  removeAccessToken :function(cond,errorcallback,successcallback){
    qb.delete('sd_access_token', cond, function(err, res) {
      if (err) errorcallback("Uh oh! Couldn't delete results: " + err);
      successcallback(true);
    });
  },
  createAccessToken :function(data,errorcallback,successcallback){
    return qb.insert('sd_access_token', data, function(err, res) {
          //  qb.release();
            if (err) errorcallback(err);
            console.log(res.insertId);
            successcallback(res.insertId);
        });
  }

}
module.exports=model;
