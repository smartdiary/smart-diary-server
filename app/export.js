module.exports={
   asyncForEach     : require('async-foreach').forEach,
   multer           : require('multer'),
   mkdirp           : require('mkdirp'),
   jwt              : require('jsonwebtoken'),
   crypto           : require('crypto'),
   fs               : require('fs'),
   path             : require('path'),
   async            : require('async'),
   jimp 			: require("jimp"),
   sizeOf           : require('image-size'),
}
