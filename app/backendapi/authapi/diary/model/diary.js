var database          = require('../../../../../config/database');
var qb = require('node-querybuilder').QueryBuilder(database, 'mysql', 'single');
var model = {
  getCategories :function(errorcallback,successcallback){
    return qb.select('id,category_name as categoryName')
      .get('sd_master_categories', function(err,response) {
          if (err) errorcallback("Uh oh! Couldn't get results: " + err.msg);
          successcallback(response);
      });
  },

  addRecord :function(data,errorcallback,successcallback){
    return qb.insert('sd_records', data, function(err, res) {
          //  qb.release();
            if (err) errorcallback(err);
            successcallback(res.insertId);
            console.log(res.insertId);
        });
  },

  updateRecord :function(data,condiction,errorcallback,successcallback){
    return qb.update('sd_records', data, condiction ,function(err, res) {
      if (err) errorcallback("Uh oh! Couldn't update results: " + err.msg);
      successcallback(true);
    });
  },

  addShare :function(data,errorcallback,successcallback){
    return qb.insert('sd_share_posts', data, function(err, res) {
          //  qb.release();
            if (err) errorcallback(err);
            successcallback(res.insertId);
        });
  },

  updateShare :function(data,cond,errorcallback,successcallback){
    return qb.update('sd_share_posts', data, cond ,function(err, res) {
      if (err) errorcallback("Uh oh! Couldn't update results: " + err.msg);
      successcallback(true);
    });
  },

  getRecordByCondiction :function(condiction,errorcallback,successcallback){
      var sql="Select * from sd_records where user_id = "+condiction.user_id+" AND  created <= (CURDATE() + INTERVAL 1 DAY) AND `status` = 1 ";
      return qb.query(sql,function(err,response){
         successcallback(response);
       })
  },

  getShareByCondiction :function(condiction,errorcallback,successcallback){
    return qb.select('*')
      .where(condiction)
      .get('sd_share_posts', function(err,response) {
          if (err) errorcallback("Uh oh! Couldn't get results: " + err.msg);
          successcallback(response);
      });
  },

  getTaggedRecordsByCondiction :function(condiction,errorcallback,successcallback){
    return qb.query("Select * from sd_records where "+condiction,function(err,response){
       successcallback(response);
     })
  },

  getRecordDetailByCondiction :function(param,errorcallback,successcallback){
    var sql="Select "+param.select+" " +param.join+ " where r.status=1 AND "+param.condiction+" r.id !=''"+param.order+" "+param.limit;
    console.log(sql);
    //successcallback(sql);
    return qb.query(sql,function(err,response){
       successcallback(response);
     })
  },

  getShareDetailByCondiction :function(param,errorcallback,successcallback){
    var sql="Select "+param.select+" " +param.join+ " where sp.status=1 AND "+param.condiction+" AND sp.id !='' "+' ORDER BY sp.id DESC '+param.limit;
    console.log(sql);
    return qb.query(sql,function(err,response){
       successcallback(response);
     })
  }
}
module.exports=model;
