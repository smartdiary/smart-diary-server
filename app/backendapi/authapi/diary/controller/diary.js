var express = require('express');
var router = express.Router();
var app = express();
var customodule = require('../../../../customexport');
var node_module = require('../../../../export');
var ERROR = require('../../../../../config/statuscode');
var Diarymodel = require('../model/diary');


router.get('/getCategories', customodule.authenticate, function(req, res, next) {
    Diarymodel.getCategories(function(err) {
        console.log(err)
    }, function(cats) {
        var response = {
            resCode: 200,
            response: cats
        }
        res.status(200).send(response);
    })
});

//*****************UPLOAD START USING ASYNC PACKAGE AND MULTER ***********//
var diarystorage = node_module.multer.diskStorage({
    destination: function(req, file, cb) {
        //var code = JSON.parse(req.body.model).empCode;
        var dest = 'public/diary/';
        node_module.mkdirp(dest, function(err) {
            console.log(dest);
            if (err) cb(err, dest);
            else cb(null, dest);
        });
    },
    filename: function(req, file, cb) {
        cb(null, Date.now() + '.' + 'jpg');
    }
});
var diaryimageupload = node_module.multer({
    storage: diarystorage
});
router.post('/createRecord', customodule.authenticate, diaryimageupload.any(), function(req, res, next) {
    var today_date = new Date();
    var datetime = today_date.getFullYear() + "-" + (today_date.getMonth() + 1) + "-" + today_date.getDate() + " " + today_date.getHours() + ":" + today_date.getMinutes() + ":" + today_date.getSeconds();
    var title = req.body.title;
    var tagged_user = req.body.taggedUsers;
    var description = req.body.description;
    var category_id = req.body.categoryId;
    var post_date = datetime;
    var user_id = req.body.userId;
    var canvasData = req.body.canvasData;
    if (req.files.length > 0) {
        var filename = req.files[0].filename;
    } else {
        var filename = '';
    }
    if (!title || !description || !category_id || !user_id) {
        res.status(200).send(ERROR.DIARY.REQUIREDFIELD);
    } else {
        customodule.helper.checkRules("recordPost", user_id, function(success) {
            if (success == 0) {
                res.status(200).send(ERROR.DIARY.RESTRICTPOST);
            } else {
                var filename = req.files[0].filename;
                var thumbFile = Date.now() + '.' + 'jpg';
                var destThumb = 'public/diary/thumb/' + filename;
                var path = 'public/diary/' + filename;

                node_module.sizeOf(path, function(err, dimensions) {
                    var imageWidth = dimensions.width;
                    var imageHeight = dimensions.height;
                    // var imageWidth = 640;
                    // var imageHeight = 960;
                    //calculation for portrait
                    //if image height grater than image width
                    if (imageHeight > imageWidth) {
                        var proportionWidth = 100;
                        var proportionHeight = (imageHeight * proportionWidth) / imageWidth;
                        proportionHeight = parseInt(proportionHeight);
                    }
                    if (imageWidth > imageHeight) {
                        var proportionHeight = 100;
                        var proportionWidth = (imageWidth * proportionHeight) / imageHeight;
                        proportionWidth = parseInt(proportionWidth);
                    }

                    console.log('original ', imageWidth, imageHeight);
                    console.log('proportion ', proportionWidth, proportionHeight);
                    node_module.jimp.read(path).then(function(lenna) {
                        lenna.resize(proportionWidth, proportionHeight)
                            .quality(80) // set JPEG quality
                            .write(destThumb); // save
                    }).catch(function(err) {
                        console.error(err);
                    });

                    var pendingPostcnt = success;
                    var data = {
                        user_id: user_id,
                        title: title,
                        tagged_user: tagged_user,
                        category_id: category_id,
                        description: description,
                        drawing: filename,
                        post_date: post_date,
                        canvas_data:canvasData
                    }
                    console.log(data);
                    Diarymodel.addRecord(data, function(err) {
                        console.log(err)
                    }, function(success) {
                        if (success) {
                            var postPending = (success == true) ? "1000" : (pendingPostcnt - 1);
                            var response = {
                                resCode: 200,
                                postPending: postPending,
                                response: "Diary created successfully"
                            }
                            res.status(200).send(response);
                        }
                    })
                })
            }
        });
    }
});

router.get('/deleteRecord', customodule.authenticate, function(req, res, next) {
    var id = req.query.id;
    var condiction = {
        id: id
    }
    var data = {
        status: '0'
    }
    if (!id) {
        res.status(200).send(ERROR.DIARY.REQUIREDFIELD);
    } else {
        Diarymodel.updateRecord(data, condiction, function(err) {
            console.log();
        }, function(updaterecord) {
            if (updaterecord) {
                var cond = {
                    record_id: id
                }
                Diarymodel.updateShare(data, cond, function(err) {
                    console.log(err)
                }, function(updateshare) {
                    var response = {
                        resCode: 200,
                        response: "Record has been deleted successfully"
                    }
                    res.status(200).send(response);
                })
            }
        })
    }
});

router.put('/updateRecord', customodule.authenticate, diaryimageupload.any(), function(req, res, next) {
    var title = req.body.title;
    var description = req.body.description;
    var category_id = req.body.categoryId;
    var record_id = req.body.recordId;
    var tagged_user = req.body.taggedUsers;
    console.log(req.body.title);

    if (!title || !description || !category_id || !record_id) {
        res.status(200).send(ERROR.DIARY.REQUIREDFIELD);
    } else {
        if (req.files.length > 0) {
            var filename = req.files[0].filename;
            var data = {
                title: title,
                category_id: category_id,
                description: description,
                tagged_user: tagged_user,
                drawing: filename
            }
        } else {
            var data = {
                title: title,
                category_id: category_id,
                description: description,
                tagged_user: tagged_user
            }
        }

        var condiction = {
            id: record_id
        }
        if (req.files.length > 0) {
            var filename = req.files[0].filename;
            var thumbFile = Date.now() + '.' + 'jpg';
            var destThumb = 'public/diary/thumb/' + filename;
            var path = 'public/diary/' + filename;
            node_module.sizeOf(path, function(err, dimensions) {
                var imageWidth = dimensions.width;
                var imageHeight = dimensions.height;
                // var imageWidth = 640;
                // var imageHeight = 960;
                //calculation for portrait
                //if image height grater than image width
                if (imageHeight > imageWidth) {
                    var proportionWidth = 100;
                    var proportionHeight = (imageHeight * proportionWidth) / imageWidth;
                    proportionHeight = parseInt(proportionHeight);
                }
                if (imageWidth > imageHeight) {
                    var proportionHeight = 100;
                    var proportionWidth = (imageWidth * proportionHeight) / imageHeight;
                    proportionWidth = parseInt(proportionWidth);
                }

                console.log('original ', imageWidth, imageHeight);
                console.log('proportion ', proportionWidth, proportionHeight);
                node_module.jimp.read(path).then(function(lenna) {
                    lenna.resize(proportionWidth, proportionHeight)
                        .quality(80) // set JPEG quality
                        .write(destThumb); // save
                }).catch(function(err) {
                    console.error(err);
                });
            });
        }
        Diarymodel.updateRecord(data, condiction, function(err) {
            console.log(err)
        }, function(success) {
            if (success) {
                var response = {
                    resCode: 200,
                    response: "Diary updated successfully"
                }
                res.status(200).send(response);
            }
        })
    }
})

//*****************UPLOAD START USING ASYNC PACKAGE AND MULTER ***********//
var diarycommentstorage = node_module.multer.diskStorage({
    destination: function(req, file, cb) {
        //var code = JSON.parse(req.body.model).empCode;
        var dest = 'public/diary/comments';
        node_module.mkdirp(dest, function(err) {
            console.log(dest);
            if (err) cb(err, dest);
            else cb(null, dest);
        });
    },
    filename: function(req, file, cb) {
        cb(null, Date.now() + '.' + 'jpg');
    }
});
var diarycommentimageupload = node_module.multer({
    storage: diarycommentstorage
});
router.post('/sharePost', customodule.authenticate, diarycommentimageupload.any(), function(req, res, next) {
    var comment = req.body.comment;
    var record_id = req.body.recordId;
    var user_id = req.body.userId;
    var filename = '';
    // customodule.authenticate,diarycommentimageupload.any(),
    /*if (req.files.length > 0) {
      var filename  = req.files[0].filename;
    }else{
      var filename  = '';
    }*/
    if (!comment || !record_id || !user_id) {
        res.status(200).send(ERROR.DIARY.REQUIREDFIELD);
    } else {
        customodule.helper.checkRules("sharePost", user_id, function(success) {
            if (success == false) {
                res.status(200).send(ERROR.DIARY.RESTRICTPOST);
            } else {
                //var condiction = "FIND_IN_SET("+user_id+", tagged_user) AND id="+record_id;
                //Diarymodel.getTaggedRecordsByCondiction(condiction,function(err){console.log(err)},function(response){
                //if(response.length>0){
                var data = {
                    user_id: user_id,
                    record_id: record_id,
                    comment: comment,
                    image: filename
                }
                Diarymodel.addShare(data, function(err) {
                        console.log(err)
                    }, function(success) {
                        if (success) {
                            var response = {
                                resCode: 200,
                                response: "Post added successfully"
                            }
                            res.status(200).send(response);
                        }
                    })
                    /*}else{
                      res.status(200).send(ERROR.DIARY.UNAUTHORIZEDPOST);
                    }*/
                    //})

            }
        })
    }
});

router.get('/deleteShare', customodule.authenticate, function(req, res, next) {
    var id = req.query.id;
    var condiction = {
        id: id
    }
    var data = {
        status: '0'
    }
    if (!id) {
        res.status(200).send(ERROR.DIARY.REQUIREDFIELD);
    } else {
        Diarymodel.updateShare(data, condiction, function(err) {
            console.log(err)
        }, function(updateshare) {
            var response = {
                resCode: 200,
                response: "Share has been deleted successfully"
            }
            res.status(200).send(response);
        })
    }
});

router.put('/updatePost', customodule.authenticate, diarycommentimageupload.any(), function(req, res, next) {
    var comment = req.body.comment;
    var record_id = req.body.recordId;
    var user_id = req.body.userId;
    var post_id = req.body.postId;

    if (!comment || !record_id || !user_id || !post_id) {
        res.status(200).send(ERROR.DIARY.REQUIREDFIELD);
    } else {
        /*if (req.files.length > 0) {
          var filename  = req.files[0].filename;
          var data =  {
            user_id     : user_id,
            record_id   : record_id,
            comment     : comment,
            image       : filename
          }
        }else{
          var data =  {
            user_id     : user_id,
            record_id   : record_id,
            comment     : comment,
          }
        }*/

        var data = {
            user_id: user_id,
            record_id: record_id,
            comment: comment,
        }
        var condiction = {
            id: post_id
        }
        Diarymodel.updateShare(data, condiction, function(err) {
            console.log(err)
        }, function(success) {
            if (success) {
                var response = {
                    resCode: 200,
                    response: "Post updated successfully"
                }
                res.status(200).send(response);
            }
        })
    }
})

router.post('/getRecords', customodule.authenticate, function(req, res, next) {
        var page_num = req.body.pageNum;
        var page_count = (req.body.pagecount) ? req.body.pagecount : '';
        var record_id = req.body.recordId;
        var user_id = req.body.userId;
        var category_id = req.body.categoryId;
        var created_start_date = req.body.createdStartDate;
        var created_end_date = req.body.createdEndDate;
        var post_start_date = req.body.postStartDate;
        var post_end_date = req.body.postEndDate;

        var date_cond = (created_start_date && created_end_date) ? "r.created >='" + created_start_date + "' AND r.created <='" + created_end_date + "' AND " : "";
        var postdate_cond = (post_start_date && post_end_date) ? "r.post_date >='" + post_start_date + "' AND r.post_date <='" + post_end_date + "' AND " : "";
        var cat_cond = (category_id) ? "r.category_id=" + category_id + " AND " : "";
        var record_cond = (record_id) ? "r.id=" + record_id + " AND " : "";
        var user_id = (user_id) ? "r.user_id=" + user_id + " AND " : "";
        var offset_value = (page_num) ? (page_num == 0) ? 0 : page_num - 1 : 0;

        var condiction = date_cond + postdate_cond + cat_cond + record_cond + user_id;
        var select = "r.id as recordId,r.category_id as categoryId,r.canvas_data as canvasData,r.title as recordTitle,r.description as recordDescription,concat('" + customodule.setting.diaryurl + "',CASE WHEN r.drawing='' THEN 'no-img.jpg' ELSE r.drawing END) as recordImage,rc.category_name as categoryName,ru.name as recordCreatedBy,ru.id as recordCreatedById, DATE_FORMAT(r.post_date, '%Y-%m-%d') as recordPostedDate, DATE_FORMAT(r.post_date,'%H:%i:%s') as recordPostedTime from sd_records as r";
        var join = "LEFT JOIN users as ru ON r.user_id=ru.id LEFT JOIN sd_master_categories as rc ON r.category_id=rc.id";
        var limit = (page_count != '') ? "LIMIT " + offset_value + "," + page_count : "";
        var order = 'ORDER BY r.id DESC';
        //var query         = "Select "+select+" from sd_records as r "++"  where status=1 AND "+condiction+" id !='' Limit "+offset_value+","+page_count;
        //console.log(condiction)
        var param = {
                condiction: condiction,
                select: select,
                join: join,
                limit: limit,
                order: order
            }
            //console.log(param);
        Diarymodel.getRecordDetailByCondiction(param, function(err) {
            console.log(err)
        }, function(records) {
            if (records.length > 0) {
                node_module.async.eachSeries(records, iteration, function(prime, callback) {
                    var response = {
                        resCode: 200,
                        response: records
                    }
                    res.status(200).send(response);
                })
            } else {
                var response = {
                    resCode: 200,
                    response: []
                }
                res.status(200).send(response);
            }
        })
        var iteration = function(row, callbackDone) {
            var select = "sp.id as sharePostId,sp.comment as comment,concat('" + customodule.setting.diarycommentsurl + "',spu.avatar) as ownerAvatar,DATE_FORMAT(sp.comment_date,'%d %b %Y') as commentDate, DATE_FORMAT(sp.comment_date,'%h:%i %p') as commentTime, spu.id as sharePostById,spu.name as sharePostBy from sd_share_posts as sp"
            var join = "Left Join users as spu ON spu.id=sp.user_id";
            var condiction = "sp.record_id=" + row['recordId'];
            var limit = "Limit 0,20";
            var param = {
                condiction: condiction,
                select: select,
                join: join,
                limit: limit
            }
            Diarymodel.getShareDetailByCondiction(param, function(err) {
                console.log(err)
            }, function(sharepost) {
                row['comments'] = sharepost;
                callbackDone();
                return;
            });
        }
    })
    // customodule.authenticate,
router.get('/getUserRecords', function(req, res, next) {
    if (!req.query.userId) {
        res.status(200).send(ERROR.DIARY.REQUIREDFIELD);
        return false;
    } else {
        var user_id = "r.user_id=" + req.query.userId + " AND ";
        var condiction = user_id;
        var select = "r.id as recordId,r.title as recordTitle,r.description as recordDescription,concat('" + customodule.setting.diarythumburl + "',CASE WHEN r.drawing='' THEN 'no-img.jpg' ELSE r.drawing END) as recordImage, DATE_FORMAT(r.post_date, '%Y-%m-%d') as recordPostedDate, DATE_FORMAT(r.post_date,'%H:%i:%s') as recordPostedTime, DATE_FORMAT(r.post_date, '%Y') as recordYear, DATE_FORMAT(r.post_date, '%m') as recordMonth  from sd_records as r";
        var join = "LEFT JOIN users as ru ON r.user_id=ru.id LEFT JOIN sd_master_categories as rc ON r.category_id=rc.id";
        var limit = "";
        var order = "ORDER BY r.id DESC"

        var param = {
                condiction: condiction,
                select: select,
                join: join,
                limit: limit,
                order: order
            }
            //console.log(param);
        customodule.helper.checkRules("recordPost", req.query.userId, function(success) {
            var postPending = (success == true) ? "1000" : (success);
            Diarymodel.getRecordDetailByCondiction(param, function(err) {
                    console.log(err)
                }, function(records) {
                    //res.send(records);
                    if (records.length > 0) {
                        var response = {
                            resCode: 200,
                            postPending: postPending,
                            records: records
                        }
                        res.status(200).send(response);
                    } else {
                        var response = {
                            resCode: 200,
                            postPending: postPending,
                            records: []
                        }
                        res.status(200).send(response);
                    }
                })
                //}
        })
    }
})

router.post('/getSharedPost', customodule.authenticate, function(req, res, next) {
    var recordId = req.body.recordId;
    var page_num = req.body.pageNum;
    var page_count = (req.body.pagecount) ? req.body.pagecount : '';
    //concat('"+customodule.setting.diarycommentsurl+"',sp.image) as sharePostImageUrl,
    var select = "sp.id as sharePostId,sp.comment,DATE_FORMAT(sp.comment_date,'%d %b %Y') as commentDate, DATE_FORMAT(sp.comment_date,'%h:%i %p') as commentTime, spu.id as sharePostById,spu.name as sharePostBy from sd_share_posts as sp"
    var join = "Left Join users as spu ON spu.id=sp.user_id";
    var condiction = "sp.record_id=" + recordId;
    var offset_value = (page_num) ? (page_num == 0) ? 0 : page_num - 1 : 0;
    var limit = (page_count != '') ? "LIMIT " + offset_value + "," + page_count : "";
    var param = {
        condiction: condiction,
        select: select,
        join: join,
        limit: limit
    }
    if (!recordId) {
        res.status(200).send(ERROR.DIARY.REQUIREDFIELD);
    } else {
        Diarymodel.getShareDetailByCondiction(param, function(err) {
            console.log(err)
        }, function(sharepost) {
            var response = {
                resCode: 200,
                response: sharepost
            }
            res.status(200).send(response);
        });
    }
})



module.exports = router;
