var database = require('../../../../../config/database');
var node_module = require('../../../../export');
var qb = require('node-querybuilder').QueryBuilder(database, 'mysql', 'single');
var model = {
    getUserByCondiction: function(condiction, errorcallback, successcallback) {
        return qb.select('*')
            .where(condiction)
            .get('users', function(err, response) {
                console.log(qb.last_query());
                if (err) errorcallback("Uh oh! Couldn't get results: " + err.msg);
                successcallback(response);
            });
    },
    updateUserByCondiction: function(data, condiction, errorcallback, successcallback) {
        return qb.update('users', data, condiction, function(err, res) {
            //qb.last_query
            if (err) errorcallback("Uh oh! Couldn't update results: " + err.msg);
            successcallback(true);
        });
    },
    getUserFullInfoByCondiction: function(param, errorcallback, successcallback) {
        return qb.query("Select " + param.select + " from users as u " + param.join + " where " + param.condiction, function(err, response) {
            successcallback(response);
        })
    },
    getAccessTokenByCondiction: function(condiction, errorcallback, successcallback) {
        return qb.select('*')
            .where(condiction)
            .get('sd_access_token', function(err, response) {
                if (err) errorcallback("Uh oh! Couldn't get results: " + err.msg);
                successcallback(response);
            });
    },
    addImg: function(data, errorcallback, successcallback) {
        return qb.insert('sd_record_albums', data, function(err, res) {
            //  qb.release();
            if (err) errorcallback(err);
            successcallback(res.insertId);
        });
    },
    getAlbumByCondiction: function(condiction, errorcallback, successcallback) {
        console.log(condiction);
        return qb.select('*')
            .where(condiction)
            .get('sd_user_albums', function(err, response) {
                console.log(qb.last_query());
                if (err) errorcallback("Uh oh! Couldn't get results: " + err.msg);
                successcallback(response);
            });
    },
    addAlbum: function(data, errorcallback, successcallback) {
        return qb.insert('sd_user_albums', data, function(err, res) {
            //  qb.release();
            if (err) errorcallback(err);
            successcallback(res.insertId);
        });
    },
    getAlbumImage: function(data, errorcallback, successcallback) {
        console.log(data);
        return qb.query("select GROUP_CONCAT(thumb) as thumbimage,GROUP_CONCAT(large) as largeimage from sd_user_albums where id IN(" + data + ")", function(err, response) {
            successcallback(response);
        })
    },
    deleteThumbAlbumImage: function(files, errorcallback, successcallback) {
        var i = files.length;
        files.forEach(function(filename) {
            var thumbPath = "public/album/thumb/" + filename;
            node_module.fs.unlink(thumbPath, function(err) {
                i--;
                if (err) {
                    errorcallback(err);
                    return;
                }
                if (i == 0) {
                    successcallback('true');
                }
            });
        });
    },
    deleteLargeAlbumImage: function(files, errorcallback, successcallback) {
        var i = files.length;
        files.forEach(function(filename) {
            var thumbPath = "public/album/" + filename;
            node_module.fs.unlink(thumbPath, function(err) {
                i--;
                if (err) {
                    errorcallback(err);
                    return;
                }
                if (i == 0) {
                    successcallback('true');
                }
            });
        });
    },
    deleteAlbumImage: function(data, errorcallback, successcallback) {
        console.log(data);
        return qb.query("DELETE FROM sd_user_albums where id IN(" + data + ")", function(err, response) {
            successcallback('true');
        })
    },
}
module.exports = model;
