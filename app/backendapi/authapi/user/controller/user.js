var express = require('express');
var router = express.Router();
var app = express();
var customodule = require('../../../../customexport');
var node_module = require('../../../../export');
var ERROR = require('../../../../../config/statuscode');
var Usermodel = require('../model/user');

router.post('/changePassword', customodule.authenticate, function(req, res, next) {
    var old_password = req.body.oldPassword;
    var new_password = req.body.newPassword;
    var email = req.body.email;
    var condiction = {
        password: node_module.crypto.createHash('md5').update(customodule.setting.passprefix + old_password).digest("hex"),
        email: email,
        status: '1'
    }
    var data = {
        password: node_module.crypto.createHash('md5').update(customodule.setting.passprefix + new_password).digest("hex"),
    }
    Usermodel.getUserByCondiction(condiction, function(err) {
        console.log(err)
    }, function(users) {
        if (users.length > 0) {
            Usermodel.updateUserByCondiction(data, condiction, function(err) {
                console.log(err)
            }, function(success) {
                if (success) {
                    var response = {
                        resCode: 200,
                        response: "You have successfully change your password"
                    }
                    res.status(200).send(response);
                }
            })
        } else {
            res.status(200).send(ERROR.USER.INVALIDCREDENTIAL);
        }
    })
})

router.get('/getUserlist', customodule.authenticate, function(req, res, next) {
    var user_type_id = req.query.userTypeId;
    var user_id = req.query.userId;
    if (user_type_id) {
        var condiction = "u.user_type_id='" + user_type_id + "' AND u.status='1'";
    } else if (user_id) {
        var condiction = "u.id ='" + user_id + "' AND u.status='1'";
    } else {
        var condiction = "u.user_type_id !='4' AND u.status='1'";
    }
    var select = "u.id as userId,ifnull(u.name,'') as userName, ifnull(u.email,'') as userEmail, ifnull(u.dob,'') as userDob,ifnull(u.gender,'') as userGender,ifnull(u.phone,'') as userMobile,ifnull(u.user_type_id,'') as userTypeId,ifnull(u.user_grouptype_id,'') as userGroupId,ifnull(u.company_name,'') as companyName,ifnull(u.company_address,'') as companyAddress,ifnull(ug.group_type,'') as userGroup,ifnull(ut.type,'') as userType,concat('" + customodule.setting.profileImgUrl + "',u.avatar) as avatarImage";
    var join = "LEFT JOIN sd_user_grouptype as ug ON ug.id=u.user_grouptype_id LEFT JOIN sd_user_type as ut ON ut.id=u.user_type_id";

    var param = {
        condiction: condiction,
        join: join,
        select: select
    }
    console.log(condiction);
    Usermodel.getUserFullInfoByCondiction(param, function(err) {
        res.status(200).send(ERROR.COMMON.SOMETHINGWRONG);
    }, function(users) {
        var response = {
            resCode: 200,
            response: users
        }
        res.status(200).send(response);
    })
});

var diarystorage = node_module.multer.diskStorage({
    destination: function(req, file, cb) {
        //var code = JSON.parse(req.body.model).empCode;
        var dest = 'public/profile/';
        node_module.mkdirp(dest, function(err) {
            console.log(dest);
            if (err) cb(err, dest);
            else cb(null, dest);
        });
    },
    filename: function(req, file, cb) {
        cb(null, Date.now() + '.' + 'jpg');
    }
});
var diaryimageupload = node_module.multer({
    storage: diarystorage
});
// customodule.authenticate,
router.put('/updateProfile', diaryimageupload.any(), function(req, res, next) {
    var id = req.body.userId;
    var name = req.body.userName;
    var gender = req.body.userGender;
    var dob = req.body.userDob;
    var phone = req.body.userPhone;
    var hobbies = req.body.hobbies;
    var userTarget = req.body.userTarget;
    // var hobbies       = '';
    // if(hobbie.length > 0){
    //     hobbies       = hobbie.join();
    // }

    var company_name = '';
    var company_address = '';
    if (!id) {
        res.status(200).send(ERROR.DIARY.REQUIREDFIELD);
    } else {
        var data = {
            name: name,
            gender: gender,
            dob: dob,
            phone: phone,
            hobbies: hobbies,
            userTarget: userTarget,
            company_name: company_name,
            company_address: company_address
        }
        var condiction = {
            id: id,
            status: '1'
        }
        Usermodel.updateUserByCondiction(data, condiction, function(err) {
            res.status(200).send(ERROR.COMMON.SOMETHINGWRONG);
        }, function(success) {
            // image upload :
            if (req.files.length > 0) {
                var filename = req.files[0].filename;
                var thumbFile = Date.now() + '.' + 'jpg';
                var destThumb = 'public/profile/thumb/' + thumbFile;
                var path = 'public/profile/' + filename;
                node_module.jimp.read(path).then(function(lenna) {
                    lenna.resize(256, 256) // resize
                        .quality(80) // set JPEG quality
                        .write(destThumb); // save
                }).catch(function(err) {
                    console.error(err);
                });

                var imgData = {
                    userId: id,
                    thumb: thumbFile,
                    large: filename
                }
                Usermodel.addImg(imgData, function(err) {
                    console.log(err)
                }, function(success) {
                    if (success) {
                        var response = {
                            resCode: 200,
                            response: "Image upload successfully"
                        }
                        res.status(200).send(response);
                    }
                })
            } else if (req.files.length == 0) {
                if (success) {
                    var response = {
                        resCode: 200,
                        response: "Profile updated successfully"
                    }
                    res.status(200).send(response);
                }
            }
        })
    }
})

var albumstorage = node_module.multer.diskStorage({
    destination: function(req, file, cb) {
        //var code = JSON.parse(req.body.model).empCode;
        var dest = 'public/album/';
        node_module.mkdirp(dest, function(err) {
            console.log(dest);
            if (err) cb(err, dest);
            else cb(null, dest);
        });
    },
    filename: function(req, file, cb) {
        cb(null, Date.now() + '.' + 'jpg');
    }
});
var albumimageupload = node_module.multer({
    storage: albumstorage
});
router.post('/postAlbum', albumimageupload.any(), function(req, res, next) {
    var id = req.body.userId;
    var condition = {
        user_id: id
    }
    Usermodel.getAlbumByCondiction(condition, function(err) {
        res.status(200).send(ERROR.COMMON.SOMETHINGWRONG);
    }, function(success) {
        // image upload :
        console.log(success.length);
        if (success.length < 10) {
            var albumPosted = success.length;

            if (req.files.length > 0) {
                var filename = req.files[0].filename;
                var thumbFile = Date.now() + '.' + 'jpg';
                var destThumb = 'public/album/thumb/' + thumbFile;
                var path = 'public/album/' + filename;

                node_module.sizeOf(path, function(err, dimensions) {
                    var imageWidth = dimensions.width;
                    var imageHeight = dimensions.height;
                    // var imageWidth = 640;
                    // var imageHeight = 960;
                    //calculation for portrait
                    //if image height grater than image width
                    if (imageHeight > imageWidth) {
                        var proportionWidth = 100;
                        var proportionHeight = (imageHeight * proportionWidth) / imageWidth;
                        proportionHeight = parseInt(proportionHeight);
                    }
                    if (imageWidth > imageHeight) {
                        var proportionHeight = 100;
                        var proportionWidth = (imageWidth * proportionHeight) / imageHeight;
                        proportionWidth = parseInt(proportionWidth);
                    }

                    console.log('original ', imageWidth, imageHeight);
                    console.log('proportion ', proportionWidth, proportionHeight);
                    node_module.jimp.read(path).then(function(lenna) {
                        lenna.resize(proportionWidth, proportionHeight)
                            .quality(80) // set JPEG quality
                            .write(destThumb); // save
                    }).catch(function(err) {
                        console.error(err);
                    });
                    var imgData = {
                        user_id: id,
                        thumb: thumbFile,
                        large: filename
                    }
                    console.log(imgData);
                    Usermodel.addAlbum(imgData, function(err) {
                        console.log(err)
                    }, function(success) {
                        if (success) {
                            var albumPending = 10 - albumPosted;
                            console.log('album pending', albumPosted);
                            var response = {
                                resCode: 200,
                                albumPending: albumPending,
                                large: "http://111.93.177.59:5000/public/album/" + filename,
                                thumb: "http://111.93.177.59:5000/public/album/thumb/" + thumbFile,
                                response: "Album image uploaded successfully",
                                albumId: success
                            }
                            res.status(200).send(response);
                        }
                    })
                });
            } else if (req.files.length == 0) {
                if (success) {
                    var response = {
                        resCode: 201,
                        response: "Please provide album image."
                    }
                    res.status(200).send(response);
                }
            }
        } else {
            var response = {
                resCode: 201,
                response: "You have exceeded the max no of post album."
            }
            res.status(200).send(response);
        }
    })

});
router.post('/deleteAlbum', function(req, res, next) {
    var albumId = req.body.albumId;
    //now split the string and delete the video one by one
    var albumArr = albumId.split(',');
    if (albumArr.length > 0) {
        Usermodel.getAlbumImage(albumId, function(err) {
            console.log(err)
        }, function(success) {
            if (success) {
                if (success.length > 0) {
                    if (success[0].thumbimage != null) {
                        var thumbarr = success[0].thumbimage.split(",");
                        var largearr = success[0].largeimage.split(",");
                        console.log(thumbarr);
                        Usermodel.deleteThumbAlbumImage(thumbarr, function(err) {
                            console.log(err)
                        }, function(success) {
                            if (success) {
                                Usermodel.deleteLargeAlbumImage(largearr, function(err) {
                                    console.log(err)
                                }, function(success) {
                                    if (success) {
                                        console.log('delete large image');
                                        Usermodel.deleteAlbumImage(albumId, function(err) {
                                            console.log(err)
                                        }, function(successimagedelete) {
                                            console.log('success value', successimagedelete);
                                            if (successimagedelete) {
                                                var response = {
                                                    resCode: 200,
                                                    response: 'Image has been deleted successfully.'
                                                }
                                            } else {
                                                var response = {
                                                    resCode: 201,
                                                    response: 'Something went wrong while delete album !'
                                                }
                                            }
                                            res.status(200).send(response);
                                        })
                                    } else {
                                        var response = {
                                            resCode: 201,
                                            response: 'Something went wrong while delete album !'
                                        }
                                        res.status(200).send(response);
                                    }
                                })
                            } else {
                                var response = {
                                    resCode: 201,
                                    response: 'Something went wrong while delete album !'
                                }
                                res.status(200).send(response);
                            }
                        })
                    } else {
                        var response = {
                            resCode: 201,
                            response: 'No album list found'
                        }
                        res.status(200).send(response);
                    }
                }
            }
        })
    } else {
        var response = {
            resCode: 201,
            response: "Sorry no album seleted !!"
        }
    }
});


module.exports = router;
