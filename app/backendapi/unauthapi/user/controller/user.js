var express = require('express');
var router = express.Router();
var app = express();
var customodule = require('../../../../customexport');
var node_module = require('../../../../export');
var ERROR = require('../../../../../config/statuscode');
var Usermodel = require('../model/user');


router.post('/childRegistration', customodule.validateapi, function(req, res, next) {
    var name = req.body.userName;
    var email = req.body.userEmail;
    var dob = req.body.userDob;
    var gender = req.body.userGender;
    var mobile = req.body.userMobile;
    var parent_email = req.body.parentEmail;
    var parent_mobile = req.body.parentMobile;
    var password = req.body.userPassword;
    var user_grouptype = req.body.userGroup;
    var avatar = (gender == 'Male') ? ((user_grouptype == '5-12') ? 'avatar-child-m.jpg' : 'avatar-adult-m.jpg') : ((user_grouptype == '5-12') ? 'avatar-child-f.jpg' : 'avatar-adultf.jpg');
    //console.log(req.body);
    //console.log("email",email);
    //console.log("gender",gender);
    //console.log("name",name);
    //console.log("dob",dob);
    //console.log("password",password);
    //console.log(avatar);
    var response = {
        resCode: 200,
        response: "You have successfully registered . Please check your mail for account verification."
    }
    if (!email || !gender || !name || !dob || !password) {
        res.status(200).send(ERROR.USER.REQUIREDFIELD);
    } else {
        var useremailcond = {
            email: email
        };
        Usermodel.getUserByCondiction(useremailcond, function(err) {
            console.log("Error", err)
        }, function(userlist) {
            //  console.log(userlist);
            if (userlist.length > 0) {
                res.status(200).send(ERROR.USER.USERALREADYEXIST);
            } else {
                var password = node_module.crypto.createHash('md5').update(customodule.setting.passprefix + req.body.userPassword).digest("hex");
                var grouptypecond = {
                    group_type: user_grouptype
                }
                Usermodel.getGrouptypeByCondiction(grouptypecond, function(err) {
                    console.log(err)
                }, function(usergroup) {
                    if (usergroup.length > 0) {
                        var user_grouptype_id = usergroup[0].id;
                        var user_age = customodule.helper.getAgeUsingDob(dob).split('-');
                        if (user_age[0] > 19) {
                            res.status(200).send(ERROR.USER.INVALIDAGE);
                        } else if (user_age[0] <= 11 && user_age[0] > 4 && user_grouptype_id == 1) {
                            if (!parent_email || !parent_mobile) {
                                res.status(200).send(ERROR.USER.EMAILPHONE);
                            } else {
                                if (parent_email == email || parent_mobile == mobile) {
                                    res.status(200).send(ERROR.USER.DUPLICATEENTRY)
                                } else {
                                    var parentcond = {
                                        email: parent_email
                                    }
                                    Usermodel.getUserByCondiction(parentcond, function(err) {
                                        console.log(err)
                                    }, function(parentlist) {
                                        if (parentlist.length > 0) {
                                            res.status(200).send(ERROR.USER.PARENTALREADYEXIST);
                                        } else {
                                            var parent_verification_token = node_module.jwt.sign({
                                                time: new Date().getTime()
                                            }, customodule.setting.jwtsecret);
                                            var pin = Math.floor((Math.random() * 100000) + 1);
                                            var parentdata = {
                                                email: parent_email,
                                                phone: parent_mobile,
                                                password: password,
                                                user_type_id: 2,
                                                verification_token: parent_verification_token,
                                                avatar: avatar
                                                //pin         : pin
                                            }
                                            /* Send Mail to parent  and insert parent data to database*/
                                            Usermodel.addUser(parentdata, function(err) {
                                                console.log("Error:: ", err);
                                            }, function(ref_id) {
                                                customodule.mail(parent_email, parent_verification_token, 'parent', req.body.userPassword);
                                                var verification_token = node_module.jwt.sign({
                                                    time: new Date().getTime()
                                                }, customodule.setting.jwtsecret);
                                                var childdata = {
                                                    name: name,
                                                    email: email,
                                                    dob: dob,
                                                    gender: gender,
                                                    phone: mobile,
                                                    user_type_id: 1,
                                                    user_grouptype_id: user_grouptype_id,
                                                    password: password,
                                                    refer_user_id: ref_id,
                                                    verification_token: verification_token,
                                                    avatar: avatar
                                                }
                                                /* Send Mail to child  and insert child data to database*/
                                                Usermodel.addUser(childdata, function(err) {
                                                    console.log("Error::", err)
                                                }, function(success) {

                                                    customodule.mail(email, verification_token, 'child', '');
                                                    res.status(200).send(response);
                                                })
                                            })
                                        }
                                    })
                                }
                            }
                        } else if (user_age[0] < 5) {
                            res.status(200).send(ERROR.USER.LESSAGE);
                        } else if (user_age[0] > 11 && user_age[0] <= 19 && user_grouptype_id == 2) {
                            var verification_token = node_module.jwt.sign({
                                time: new Date().getTime()
                            }, customodule.setting.jwtsecret);
                            var childdata = {
                                name: name,
                                email: email,
                                dob: dob,
                                gender: gender,
                                phone: mobile,
                                user_type_id: 2,
                                user_grouptype_id: user_grouptype_id,
                                password: password,
                                verification_token: verification_token,
                                avatar: avatar
                            }
                            /* Send Mail to child  and insert child  data to database*/
                            Usermodel.addUser(childdata, function(err) {
                                console.log("Error::", err)
                            }, function(success) {
                                customodule.mail(email, verification_token, 'child', '');
                                res.status(200).send(response);
                            })
                        } else {
                            res.status(200).send(ERROR.USER.INVALIDGROUPDOB);
                        }
                    } else {
                        res.status(200).send(ERROR.USER.INVALIDGROUPTYPE);
                    }
                });
            }
        });
    }
});

router.get('/emailverify', function(req, res, next) {
    var token_data = req.query.token;
    var usercond = {
        verification_token: token_data
    }
    Usermodel.getUserByCondiction(usercond, function(err) {
        console.log(err)
    }, function(users) {
        if (users.length > 0) {
            var data = {
                status: '1',
                verification_token: ''
            }
            Usermodel.updateUserByCondiction(data, usercond, function(err) {
                console.log(err)
            }, function(success) {
                //res.sendFile(node_module.path.join(__dirname+'/../../../../../views/success.html'));
                node_module.fs.readFile(__dirname + '/../../../../../views/success.html', function(err, text) {
                    res.writeHead(200, {
                        'Content-Type': 'text/html'
                    });
                    res.write(text);
                    res.end();
                });
            })
        } else {
            //res.sendFile(node_module.path.join(__dirname+'/../../../../../views/failure.html'));
            node_module.fs.readFile(__dirname + '/../../../../../views/failure.html', function(err, text) {
                res.writeHead(200, {
                    'Content-Type': 'text/html'
                });
                res.write(text);
                res.end();
            });
        }
    })
});

//customodule.validateapi,
router.post('/childLogin', customodule.validateapi, function(req, res, next) {
    var email = req.body.userEmail;
    var pwd = customodule.setting.passprefix + req.body.userPassword;
    var password = node_module.crypto.createHash('md5').update(pwd).digest("hex");
    //var userpin       = req.body.userPin;

    if (!req.body.userEmail || !req.body.userPassword) {
        res.status(200).send(ERROR.USER.REQUIREDFIELD);
    } else {
        Usermodel.getUserByCondiction({
            email: email,
            password: password,
            status: '1'
        }, function(err) {
            console.log(err)
        }, function(userdata) {
            if (userdata.length > 0) {
                if (userdata[0].refer_user_id == null) {
                    var condiction = "u.email='" + email + "' AND u.password='" + password + "' AND u.status='1'";
                    var select = "u.id as userId,ifnull(u.name,'') as userName, ifnull(u.email,'') as userEmail, ifnull(u.dob,'') as userDob,ifnull(u.gender,'') as userGender,ifnull(u.phone,'') as userMobile, ifnull(ut.type,'') as userType, concat('" + customodule.setting.profileImgUrl + "',u.avatar) as avatarImage, ifnull(u.hobbies,'') as hobbies, ifnull(u.userTarget,'') as userTarget";
                    var join = "LEFT JOIN sd_user_grouptype as ug ON ug.id=u.user_grouptype_id LEFT JOIN sd_user_type as ut ON ut.id=u.user_type_id";
                } else {
                    var condiction = "u.email='" + email + "' AND u.password='" + password + "' AND u.status='1'";
                    var select = "u.id as userId,ifnull(u.name,'') as userName, ifnull(u.email,'') as userEmail, ifnull(u.dob,'') as userDob,ifnull(u.gender,'') as userGender,ifnull(u.phone,'') as userMobile,ifnull(pu.email,'') as parentEmail, ifnull(pu.phone,'') as parentMobile, CASE WHEN ug.group_type = '5-12' THEN 'child' ELSE 'adult' END as userType, concat('" + customodule.setting.profileImgUrl + "',u.avatar) as avatarImage, ifnull(u.hobbies,'') as hobbies, ifnull(u.userTarget,'') as userTarget";
                    var join = "LEFT JOIN users as pu ON pu.id=u.refer_user_id LEFT JOIN sd_user_grouptype as ug ON ug.id=u.user_grouptype_id LEFT JOIN sd_user_type as ut ON ut.id=u.user_type_id"
                }
                var param = {
                    condiction: condiction,
                    join: join,
                    select: select
                }
                console.log(condiction);
                //console.log(param.condiction);
                var row = [];
                Usermodel.getUserFullInfoByCondiction(param, function(err) {
                    console.log("Error in login::", err)
                }, function(user) {
                    if (user.length > 0) {
                        var access_token = node_module.jwt.sign({
                            time: new Date().getTime()
                        }, customodule.setting.jwtsecret);

                        //CODE MODIFIED BY DIPTI ON 10/04/2017, AS DISCUSSED USER CAN LOGIN IN DIFFERENT DEVICES
                        //var access_token =  user[0].token;

                        /* Remove previous access_token and created a new one */
                        var tokencond = {
                            user_id: user[0].userId
                        }
                        Usermodel.getAccessToken(tokencond, function(err) {}, function(success) {
                            if (success.length > 0) {
                                var access_token = success[0].token;
                            } else {
                                var access_token = node_module.jwt.sign({
                                    time: new Date().getTime()
                                }, customodule.setting.jwtsecret);
                            }
                            Usermodel.removeAccessToken(tokencond, function(err) {}, function(removesuccess) {
                                var token_data = {
                                    user_id: user[0].userId,
                                    token: access_token
                                }
                                console.log(token_data);
                                var select = "ra.id as albumId, concat('" + customodule.setting.profileImgUrl + "/thumb/',ra.thumb) as thumb, concat('" + customodule.setting.profileImgUrl + "',ra.large) as large from sd_record_albums as ra"
                                var join = "Left Join users as u ON u.id=ra.userId";
                                var condiction = "ra.userId=" + token_data.user_id;

                                var param = {
                                    condiction: condiction,
                                    select: select,
                                    join: join
                                }
                                //console.log(param);
                                Usermodel.getAlbumDetailByCondiction(param, function(err) {
                                    console.log(err)
                                }, function(albumFile) {
                                    Usermodel.createAccessToken(token_data, function(err) {}, function(success) {
                                        Usermodel.getUserAlbumList(token_data.user_id, function(err) {}, function(albumres) {
                                            user[0]['albumLimit'] = 10;
                                            user[0]['albumFiles'] = albumres;
                                            // if(user[0]['hobbies']){
                                            //   user[0]['hobbies']    = user[0]['hobbies'].split(",");
                                            // } else {
                                            //   user[0]['hobbies']    = [];
                                            // }
                                            var response = {
                                                resCode: 200,
                                                token: access_token,
                                                response: user[0]
                                            }
                                            res.status(200).send(response);
                                        })
                                    })
                                });
                                //CONDITION END FOR remove ACCESS TOKEN
                            });
                            //CONDITION END FOR GET ACCESS TOKEN
                        });
                    } else {
                        res.status(200).send(ERROR.USER.INVALIDCREDENTIAL);
                    }
                })

            } else {
                res.status(200).send(ERROR.USER.INVALIDCREDENTIAL);
            }
        });

    }
})

router.post('/forgotPassword', customodule.validateapi, function(req, res, next) {
    var condiction = {
        email: req.body.email,
        status: '1'
    }
    if (!req.body.email) {
        res.status(200).send(ERROR.USER.EMAILREQUIRED);
    } else {
        Usermodel.getUserByCondiction(condiction, function(err) {
            console.log(err)
        }, function(users) {
            if (users.length > 0) {
                var verification_token = node_module.jwt.sign({
                    time: new Date().getTime()
                }, customodule.setting.jwtsecret);
                var data = {
                    verification_token: verification_token
                }
                Usermodel.updateUserByCondiction(data, condiction, function(err) {
                    console.log(err)
                }, function(success) {
                    customodule.mail(req.body.email, verification_token, 'forgotPassword', '');
                    var response = {
                        resCode: 200,
                        response: "Please check your mail for generating new password."
                    }
                    res.status(200).send(response);
                })
            } else {
                res.status(200).send(ERROR.USER.INVALIDEMAIL);
            }
        });
    }

})

router.get('/resetPassword', function(req, res, next) {
    var token_data = req.query.token;
    var usercond = {
        verification_token: token_data,
        status: '1'
    }

    Usermodel.getUserByCondiction(usercond, function(err) {
        console.log(err)
    }, function(users) {
        if (users.length > 0) {
            res.setHeader('Content-Type', 'text/html');
            res.render(node_module.path.join(__dirname + '/../../../../../views/forgotPassword.ejs'), {
                verification_token: token_data
            });
        } else {
            res.setHeader('Content-Type', 'text/html');
            res.render(node_module.path.join(__dirname + '/../../../../../views/failure.html'));
        }
    })
});

router.post('/reset', function(req, res, next) {
    var token = req.body.token;
    var usercond = {
        verification_token: token
    }
    Usermodel.getUserByCondiction(usercond, function(err) {
        console.log(err)
    }, function(users) {
        if (users.length > 0 && token != '') {
            var password = node_module.crypto.createHash('md5').update(customodule.setting.passprefix + req.body.new_password).digest("hex");
            var cond = {
                verification_token: token,
                status: '1'
            }
            var data = {
                verification_token: '',
                password: password
            }
            Usermodel.updateUserByCondiction(data, cond, function(err) {
                console.log(err)
            }, function(success) {
                res.sendFile(node_module.path.join(__dirname + '/../../../../../views/success.html'));
            })
        } else {
            res.sendFile(node_module.path.join(__dirname + '/../../../../../views/failure.html'));
        }
    })

})


module.exports = router;
