var ERROR={
      USER:{
        INVALIDAGE:{
          "resCode":201,
          "response":"Age should be less then 20"
        },
        EMAILPHONE:{
          "resCode":201,
          "response":"Parent Email and Phone required"
        },
        LESSAGE:{
          "resCode":201,
          "response":"User age should be more then 5"
        },
        REQUIREDFIELD:{
          "resCode":201,
          "response":"Please give all required fields"
        },
        EMAILREQUIRED:{
          "resCode":201,
          "response":"Email address required"
        },
        INVALIDEMAIL:{
          "resCode":201,
          "response":"Invalid email address"
        },
        INVALIDLOGINCREDENTIAL:{
          "resCode":201,
          "response":"Your account is still not activated. Please check your mail and follow the instructions."
        },
        INVALIDCREDENTIAL:{
          "resCode":201,
          "response":"Invalid login credetial. Please provide valid credential."
        },
        INVALIDGROUPTYPE:{
          "resCode":201,
          "response":"Invalid group type"
        },
        INVALIDGROUPDOB:{
          "resCode":201,
          "response":"Invalid group type as per date of birth"
        },
        INVALIDUSERTYPE:{
          "resCode":201,
          "response":"Invalid user type"
        },
        USERALREADYEXIST:{
          "resCode":201,
          "response":"This email already exist. Please check your email for account validation or do login if already verified."
        },
        PARENTALREADYEXIST:{
          "resCode":201,
          "response":"Parent email already exist"
        },
        DUPLICATEENTRY:{
          "resCode":201,
          "response":"Parent email and mobile should be diffrent from user email and mobile."
        },
        INVALIDCREDENTIAL:{
          "resCode":201,
         "response":"Invalid login credential ! " 
        }
      },
      DIARY:{
        REQUIREDFIELD :{
          "resCode":201,
          "response":"Please give all required fields ."
        },
        RESTRICTPOST :{
          "resCode":201,
          "response":"You have exceed the maximum limit to post."
        },
        UNAUTHORIZEDPOST :{
          "resCode":201,
          "response":"You are not authorised to post."
        }
      },
      COMMON:{
        SOMETHINGWRONG:{
          "resCode":201,
          "response":"Something went wrong.Pleas try again"
        }
      },
      CATEGORY:{
        ALREADYEXIST:{
          "resCode":201,
          "response":"Category already exist"
        },
        BELONGSTO:{
          "resCode":201,
          "response":"User already belongs to this Category.So you are not allowed to delete this category"
        }
      }

};
module.exports=ERROR;
