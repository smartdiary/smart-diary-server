/*
 *CODE FOR SEND EMAIL
 */
var config = require("../config/setting");
var mailer = require("nodemailer");
// Use Smtp Protocol to send Email
var smtpTransport = mailer.createTransport("SMTP", {
    service: "Gmail",
    auth: {
        user: "smartdiaryapp@gmail.com",
        pass: "Rohit@12345"
    }
});
function sendMail(email,otp,type, pin){
  switch (type) {
    case "parent":
    var mailcontent = {
            from: 'rohit@gmail.com', // sender address
            to: email,
            subject: "SMART DIARY: LOGIN CREDENTIAL",
            html: "<p>Hi Parent, Please use this below link to verify your email</p><br><p>LINK:"+config.baseurl+"unauthUser/emailverify?token="+otp+".</p><p>Please use this password to login <b>"+pin+"<b> </p>"
        }
        send(mailcontent);
        break;
    case "child":
    var mailcontent = {
            from: 'rohit@gmail.com', // sender address
            to: email,
            subject: "SMART DIARY: LOGIN CREDENTIAL",
            html: "<p>Hi User, Please use this below link to verify your email</p><br><p>LINK:"+config.baseurl+"unauthUser/emailverify?token="+otp
        }
        send(mailcontent);
        break;
    case "frontuser":
    var mailcontent = {
            from: 'rohit@gmail.com', // sender address
            to: email,
            subject: "SMART DIARY: LOGIN CREDENTIAL",
            html: "<p>Hi User, Please use this below link to verify your email</p><br><p>LINK:"+config.baseurl+"unauthUser/emailverify?token="+otp
        }
        send(mailcontent);
        break;
    case "forgotPassword":
    var mailcontent = {
            from: 'rohit@gmail.com', // sender address
            to: email,
            subject: "SMART DIARY: FORGOT PASSWORD",
            html: "<p>Hi User, Please use this below link to change your password</p><br><p>LINK:"+config.baseurl+"unauthUser/resetPassword?token="+otp
        }
        send(mailcontent);
        break;
    case "emailverify":
    var mailcontent = {
            from: 'rohit@gmail.com', // sender address
            to: email,
            subject: "SMART DIARY: OTP Verification",
            html: "<p>Hi , Your OTP is confidential .Please use this otp to verify your email address</p><br><b>OTP :</b>"+otp
        }
        send(mailcontent);
        break;
}
}
function send(mailcontent){
  smtpTransport.sendMail(mailcontent, function(error, response) {
      if (error) {
          console.log(error);
      } else {
          console.log("Message sent: " + response.message);
      }
      smtpTransport.close();
  });
}
module.exports=sendMail;
